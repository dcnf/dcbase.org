<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use DCBase\ClassLoader;
use DCBase\ConfigLoader;

//  locale stuff
setlocale(LC_CTYPE, 'en_US.UTF-8');
mb_internal_encoding('UTF-8');
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('UTC');

// php environment constants
define('DCBASE_MEMORY_START', memory_get_usage());
define('DCBASE_TIME_START', microtime(true));

// paths
if (!defined('DCBASE_ROOT_PATH')) define('DCBASE_ROOT_PATH', __DIR__ . '/');
if (!defined('DCBASE_VENDOR_PATH')) define('DCBASE_VENDOR_PATH', DCBASE_ROOT_PATH . 'vendor/');

if (!defined('DCBASE_CACHE_PATH')) define('DCBASE_CACHE_PATH', DCBASE_ROOT_PATH . 'cache/');
if (!defined('DCBASE_CONFIG_PATH')) define('DCBASE_CONFIG_PATH', DCBASE_ROOT_PATH . 'config/');
if (!defined('DCBASE_TEMPLATE_PATH')) define('DCBASE_TEMPLATE_PATH', DCBASE_ROOT_PATH . 'template/');
if (!defined('DCBASE_EXTENSION_PATH')) define('DCBASE_EXTENSION_PATH', DCBASE_ROOT_PATH . 'extension/');
if (!defined('DCBASE_CONTENT_ROOT')) define('DCBASE_CONTENT_ROOT', DCBASE_ROOT_PATH . 'content/');

// Autoloader
require(DCBASE_VENDOR_PATH . 'DCBase/ClassLoader.' . PHP_EXT);
ClassLoader::create(array(
	'Parsedown'			=> DCBASE_VENDOR_PATH . 'Parsedown/Parsedown.' . PHP_EXT,
	'ParsedownExtra'	=> DCBASE_VENDOR_PATH . 'Parsedown/ParsedownExtra.' . PHP_EXT
), true)->addDefaultPaths()->addPaths(DCBASE_EXTENSION_PATH, 'DCBase\Extension');

// Load constants file
ConfigLoader::load('Constants');
