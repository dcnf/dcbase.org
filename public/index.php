<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
define('IN_DCBASE', true);
define('DCBASE_ROOT_PATH', realpath('../') . '/');
define('PHP_EXT', substr(strrchr(__FILE__, '.'), 1));
define('DEBUG', false);

use DCBase\FrontController;
use DCBase\WebRequest;
use DCBase\WebResponse;

require(DCBASE_ROOT_PATH . 'bootstrap.' . PHP_EXT);

// process the request and send a response
$response = FrontController::create()->run(WebRequest::create());
if ($response instanceof WebResponse)
	$response->send();

exit(0);
