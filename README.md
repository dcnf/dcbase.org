# Who are we?

The Direct Connect Network Foundation (DCNF) is a non-profit organization that
aims to improve the DC network by improving software, protocols and other
services in the network. The organization intends to increase the knowledge of
the network for other current non-users as well as improving the state of it for
current users.

You can help with either donating (Paypal, see below) to the organization
directly or be improving the DC network. You may also wish to become a member.
This can be done by following the actions below. Members can vote in meetings
and will also be able to request DC services and projects in the future. Each
month the project will announce the financial situation (what has been the
income and what has been bought). Note that none of the board members receive
money, everything goes directly to the organization's ventures.

# Details about the organization

DCNF is a registered non-profit organization in Sweden, with the registered
government number 802492-9716. The organization's by-laws can be found at
<https://www.dcbase.org/bylaws> and the public documents for its annual and
monthly meetings at <https://www.dcbase.org/meetings>.

# How to become a member?

To become a member, you will need to do the following:

1. Read the by-laws at <https://www.dcbase.org/bylaws> and agree to them.
2. Send a mail to dcnf (at) dcbase.org with the title [MEMBER-REQUEST]. In the
mail, specify the registering name or nick name you wish to register as. You do
not need to disclose your real name, a pseudonym is OK.
3. You will receive a mail that confirms your request. (This step is optional,
only used in the case where there is an error with the payment etc).
4. Send €10.00 to dcnf (at) dcbase.org (PayPal) with an identifying e-mail or
name.
5. You will receive a mail that confirms your payment and your inclusion in the
organization.

# Donations

Donated money will go to paying for the infrastructure (domains, servers) used
to support the operation of the organization. PayPal e-mail for donations and
member fees is dcnf (at) dcbase.org.

# Questions? How to get involved.

If you have questions please consult the FAQ section on the organization's
website at <https://www.dcbase.org/faq>. You can be involved with DCNF without
necessarily being a member of the organization for more information and examples
on potential ways to contribute look at the Contribute section on the
organization's website here <https://www.dcbase.org/contribute>.
