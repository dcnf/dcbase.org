<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

/**
 * This file should contain a mapping of dynamic paths to handler classes, if any
 *
 * Top-level array keys contain the pattern to be matched, where <name> represents
 * a named path segment and enclosing a section in parentheses makes it optional.
 *
 * Note: since this file contains references to php classes it should not be loaded before
 * the autoloader has been registered (see: bootstrap.php)
 */
return array(
	// hardcoded files + fallback files for subdomains (robots.txt, favicon.ico)
	'/<filename>' => array(
		// required: set the handler for this dynamic path, must be a subclass of \DCBase\Handler\AbstractHandler
		'handler' => \DCBase\Handler\ExtFileHandler::class,

		// required: default arguments, sets expected variable type
		'defaults' => array(
			'filename'	=> '',
		),

		// optional: override the regex pattern used to match the named argument, the patterns are automatically enclosed in a capture group
		'regex' => array(
			// these are files that should always exists
			'filename'			=> '(?:DCBase/)?LICENSE|(?:DCBase/)?README|robots\.txt|favicon\.ico',
		)
	),

	// git hooks
	'/hook/git/<hook>(/<api_token>)' => array(
		// required: set the handler for this dynamic path, must be a subclass of \DCBase\Handler\AbstractHandler
		'handler' => \DCBase\Handler\GitHandler::class,

		// required: default arguments, sets expected variable type
		'defaults' => array(
			'hook'		=> '',
			'api_token'	=> ''
		),

		// optional: override the regex pattern used to match the named argument, the patterns are automatically enclosed in a capture group
		'regex' => array(
			// only allow alphanumeric characters and underscore in the hook name
			'hook'			=> '\w+',
			// only accept valid GUID format as api_token
			'api_token'		=> '(?:\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}(?:\}){0,1}',
		)
	),

	// --- EXTENSIONS BELOW THIS LINE ---

	'/extension/hello(/<name>)' => array(
		// required: set the handler for this dynamic path, must be a subclass of \DCBase\Handler\AbstractHandler
		'handler' => \DCBase\Extension\HelloWorld\HelloHandler::class,
		// required: default arguments, sets expected variable type
		'defaults' => array('name' => 'World'),
		// optional: override the regex pattern used to match the named argument, the patterns are automatically enclosed in a capture group
		'regex' => array('name' => '\w++')
	),

	'/<filename>.log' => array(
		// required: set the handler for this dynamic path, must be a subclass of \DCBase\Handler\AbstractHandler
		'handler' => \DCBase\Extension\LogViewer\LogHandler::class,
		// required: default arguments, sets expected variable type
		'defaults' => array('filename' => ''),
		// optional: override the regex pattern used to match the named argument, the patterns are automatically enclosed in a capture group
		'regex' => array('filename' => '[^:\*\?<>\|"\n]+')
	),
);
