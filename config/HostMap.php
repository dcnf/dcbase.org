<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

/**
 * This file should contain a map of domains/hostnames to desired document roots
 */
return array(
	// 'domain.tld'				=> '/path/to/documents/',
	// 'subdomain.domain.tld'	=> '/path/to/documents/'
	'adc.dcbase.org'			=> DCBASE_CONTENT_ROOT . 'projects/adc/',
	'geoip.dcbase.org'		=> DCBASE_CONTENT_ROOT . 'projects/geoip/',
	'hublist.dcbase.org'		=> DCBASE_CONTENT_ROOT . 'projects/hublist/',
	'nmdc.dcbase.org'			=> DCBASE_CONTENT_ROOT . 'projects/nmdc/',
	'projects.dcbase.org'		=> DCBASE_CONTENT_ROOT . 'projects/'
);
