<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

// extension to a (probable) mime-type mapping array
// @todo check that this map is actually reflective of current standards and practices
return array(
	// Image files
	'bmp'		=> array('download' => false,	'content_type' => 'image/bmp'),
	'gif'		=> array('download' => false,	'content_type' => 'image/gif'),
	'jpeg'		=> array('download' => false,	'content_type' => 'image/jpeg'),
	'jpg'		=> array('download' => false,	'content_type' => 'image/jpeg'),
	'jpe'		=> array('download' => false,	'content_type' => 'image/jpeg'),
	'png'		=> array('download' => false,	'content_type' => 'image/png'),
	'tiff'		=> array('download' => false,	'content_type' => 'image/tiff'),
	'tif'		=> array('download' => false,	'content_type' => 'image/tiff'),
	'svg'		=> array('download' => false,	'content_type' => 'image/svg+xml'),

	// Text formats, the many faces of markdown (or rather the ones that make sense)
	'md'		=> array('download' => false,	'content_type' => 'text/markdown'),
	'markdown'	=> array('download' => false,	'content_type' => 'text/markdown'),

	// Twig, serve as text/plain
	'twig'		=> array('download' => false,	'content_type' => 'text/plain'),

	// Text formats
	'eml'		=> array('download' => false,	'content_type' => 'message/rfc822'),
	'css'		=> array('download' => false,	'content_type' => 'text/css'),
	'html'		=> array('download' => false,	'content_type' => 'text/html'),
	'htm'		=> array('download' => false,	'content_type' => 'text/html'),
	'shtml'		=> array('download' => false,	'content_type' => 'text/html'),
	'txt'		=> array('download' => false,	'content_type' => 'text/plain'),
	'text'		=> array('download' => false,	'content_type' => 'text/plain'),
	'log'		=> array('download' => false,	'content_type' => 'text/plain'),
	'rtx'		=> array('download' => false,	'content_type' => 'text/richtext'),
	'xml'		=> array('download' => false,	'content_type' => 'text/xml'),
	'xsl'		=> array('download' => false,	'content_type' => 'text/xml'),
	'json'		=> array('download' => false,	'content_type' => 'application/json'),
	'xhtml'		=> array('download' => false,	'content_type' => 'application/xhtml+xml'),
	'xht'		=> array('download' => false,	'content_type' => 'application/xhtml+xml'),
	'js'		=> array('download' => false,	'content_type' => 'application/javascript'),
	'ai'		=> array('download' => false,	'content_type' => 'application/postscript'),
	'eps'		=> array('download' => false,	'content_type' => 'application/postscript'),
	'ps'		=> array('download' => false,	'content_type' => 'application/postscript'),
	'csv'		=> array('download' => true,	'content_type' => 'text/x-comma-separated-values'),
	'rtf'		=> array('download' => true,	'content_type' => 'text/rtf'),

	// PHP source files
	'php'		=> array('download' => true,	'content_type' => 'application/x-httpd-php'),
	'php4'		=> array('download' => true,	'content_type' => 'application/x-httpd-php'),
	'php3'		=> array('download' => true,	'content_type' => 'application/x-httpd-php'),
	'phtml'		=> array('download' => true,	'content_type' => 'application/x-httpd-php'),
	'phps'		=> array('download' => true,	'content_type' => 'application/x-httpd-php-source'),

	// Compressed files
	'tar'		=> array('download' => true,	'content_type' => 'application/x-tar'),
	'tgz'		=> array('download' => true,	'content_type' => 'application/x-tar'),
	'zip'		=> array('download' => true,	'content_type' => 'application/x-zip'),
	'gtar'		=> array('download' => true,	'content_type' => 'application/x-gtar'),
	'gz'		=> array('download' => true,	'content_type' => 'application/x-gzip'),
	'7z'		=> array('download' => true,	'content_type' => 'application/x-7z-compressed'),

	// Audio files
	'mid'		=> array('download' => true,	'content_type' => 'audio/midi'),
	'midi'		=> array('download' => true,	'content_type' => 'audio/midi'),
	'mpga'		=> array('download' => true,	'content_type' => 'audio/mpeg'),
	'mp2'		=> array('download' => true,	'content_type' => 'audio/mpeg'),
	'mp3'		=> array('download' => true,	'content_type' => 'audio/mpeg'),
	'aif'		=> array('download' => true,	'content_type' => 'audio/x-aiff'),
	'aiff'		=> array('download' => true,	'content_type' => 'audio/x-aiff'),
	'aifc'		=> array('download' => true,	'content_type' => 'audio/x-aiff'),
	'ram'		=> array('download' => true,	'content_type' => 'audio/x-pn-realaudio'),
	'rm'		=> array('download' => true,	'content_type' => 'audio/x-pn-realaudio'),
	'rpm'		=> array('download' => true,	'content_type' => 'audio/x-pn-realaudio-plugin'),
	'ra'		=> array('download' => true,	'content_type' => 'audio/x-realaudio'),
	'wav'		=> array('download' => true,	'content_type' => 'audio/x-wav'),

	// Video files
	'rv'		=> array('download' => true,	'content_type' => 'video/vnd.rn-realvideo'),
	'mpeg'		=> array('download' => true,	'content_type' => 'video/mpeg'),
	'mpg'		=> array('download' => true,	'content_type' => 'video/mpeg'),
	'mpe'		=> array('download' => true,	'content_type' => 'video/mpeg'),
	'qt'		=> array('download' => true,	'content_type' => 'video/quicktime'),
	'mov'		=> array('download' => true,	'content_type' => 'video/quicktime'),
	'avi'		=> array('download' => true,	'content_type' => 'video/x-msvideo'),
	'movie'		=> array('download' => true,	'content_type' => 'video/x-sgi-movie'),

	// Office document formats
	'doc'		=> array('download' => true,	'content_type' => 'application/msword'),
	'xl'		=> array('download' => true,	'content_type' => 'application/excel'),
	'xls'		=> array('download' => true,	'content_type' => 'application/excel'),
	'ppt'		=> array('download' => true,	'content_type' => 'application/powerpoint'),
	'docx'		=> array('download' => true,	'content_type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	'xlsx'		=> array('download' => true,	'content_type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
	'pptx'		=> array('download' => true,	'content_type' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),

	// Misc random or proprietary file formats
	'hqx'		=> array('download' => true,	'content_type' => 'application/mac-binhex40'),
	'cpt'		=> array('download' => true,	'content_type' => 'application/mac-compactpro'),
	'psd'		=> array('download' => true,	'content_type' => 'application/x-photoshop'),
	'oda'		=> array('download' => true,	'content_type' => 'application/oda'),
	'pdf'		=> array('download' => false,	'content_type' => 'application/pdf'),
	'smi'		=> array('download' => true,	'content_type' => 'application/smil'),
	'smil'		=> array('download' => true,	'content_type' => 'application/smil'),
	'mif'		=> array('download' => true,	'content_type' => 'application/vnd.mif'),
	'wbxml'		=> array('download' => true,	'content_type' => 'application/wbxml'),
	'wmlc'		=> array('download' => true,	'content_type' => 'application/wmlc'),
	'dcr'		=> array('download' => true,	'content_type' => 'application/x-director'),
	'dir'		=> array('download' => true,	'content_type' => 'application/x-director'),
	'dxr'		=> array('download' => true,	'content_type' => 'application/x-director'),
	'dvi'		=> array('download' => true,	'content_type' => 'application/x-dvi'),
	'swf'		=> array('download' => true,	'content_type' => 'application/x-shockwave-flash'),
	'sit'		=> array('download' => true,	'content_type' => 'application/x-stuffit')
);
