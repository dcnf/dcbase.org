<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

/**
 * This file will contain the bits of site configuration that MUST NOT be public
 *
 * This file is a template and should be edited and saved as SecretSauce.php in this directory.
 */
return array(
	'git' => array(
		// This should be any valid unique (shocking!) GUID, should be configured as part of the gitlab webhook urls when pinging us (see also HandlerMap)
		'webhooks_token'	=> 'GUID HERE',
		// NOT USED: This should be an authorization token, if you want to use their REST API (Account should have at most reporter access)
		'api_key'			=> 'SECRET TOKEN'
	)
);
