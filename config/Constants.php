<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;
if (defined('DCBASE_CONSTANTS_LOADED')) return true;

// Replace IP address for the one from cloudflare
define('IP_HEADER', 'CF-Connecting-IP');

// Define these to add git matadata to templates
define('GIT_BIN_PATH', '/usr/bin/git');
define('GIT_CACHE_PATH', DCBASE_CACHE_PATH . 'git_cache/');
define('GIT_PUBLIC_URL', '//gitlab.com/dcnf/dcbase.org');

// Define this to tell the world about the middleware layer
define('DCBASE_EXPOSE', true);

// Define the log file location for the devhub chat (must reside under DCBASE_ROOT_PATH)
define('DCLOG_DEVHUB_FILE', DCBASE_ROOT_PATH . 'public/hub-history.txt');

// the following will make ConfigLoader happy :)
define('DCBASE_CONSTANTS_LOADED', true);
return true;
