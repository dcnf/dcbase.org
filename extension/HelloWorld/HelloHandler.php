<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Extension\HelloWorld;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use SplFileInfo;

use DCBase\Handler\AbstractHandler;
use DCBase\WebRequest;
use DCBase\Response\ResponseHelper;
use DCBase\WebResponse;

/** 
 * The obligatory hello world sample
 *
 * Basic config block for `config/HandlerMap.php`:
 * 
 *  ```php
 *	'/extension/hello(/<name>)' => array(
 *		// required: set the handler for this dynamic path, must be a subclass of \DCBase\Handler\AbstractHandler
 *		'handler' => \DCBase\Extension\HelloWorld\HelloHandler::class,
 *		// required: default arguments, sets expected variable type
 *		'defaults' => array('name' => 'World'),
 *		// optional: override the regex pattern used to match the named argument, the patterns are automatically enclosed in a capture group
 *		'regex' => array('name' => '\w++')
 *	),
 *	```
 */
class HelloHandler extends AbstractHandler
{
	/**
	 * Handle an incoming request for a resource outside of FrontController
	 * 
	 * @param  WebRequest       $request       The incoming request expected to be handled
	 * @param  string           $relative_path The relative file path as part of the query string
	 * @param  SplFileInfo|null $file          File object bound to a document root, referenced path may not exist and cannot be a symlink
	 * @return mixed                           Valid WebResponse instance to be served, any other values (e.g. false) will continue processing of the request in FrontController
	 */
	public function handleResource(WebRequest $request, string $relative_path, ?SplFileInfo $file = null) : ?WebResponse
	{
		// EXAMPLE ONLY: since this does nothing intelligent we don't want to handle paths that would actully be served by FrontController
		if ($file->isFile())
			return false;

		return WebResponse::text($request, 'Hello '. ucfirst($request->getPathSegment('name')) .'!');
	}
}
