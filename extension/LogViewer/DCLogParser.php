<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Extension\LogViewer;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use DCBase\Twig\TextFormat\Parser\ParserInterface;
use DCBase\Common\LinkifyText;

/** 
 * Parser implementation for the DCBase TextFormat twig extension
 */
class DCLogParser implements ParserInterface
{
	const TIMESTAMP_REGEX = '(\[(?:\d{4}-\d{2}-\d{2}[T ])?\d{2}:\d{2}(?:\:\d{2})?(?: ?[\+-]\d{2}:?\d{2})?\] ?)?';

	protected $line_separator;

	public function __construct($line_separator = PHP_EOL)
	{
		$this->line_separator = $line_separator;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNames() : array
	{
		return array('DCLog');
	}

	/**
	 * {@inheritdoc}
	 */
	public function parseContent($content) : string
	{
		$fp = fopen("php://temp", 'r+');
		if ($fp === false)
			return $content;

		if (fwrite($fp, $content) === false || !rewind($fp))
		{
			fclose($fp);
			return $content;
		}

		unset($content);
		$content = '';

		$line_num = 0;
		while($line = fgets($fp))
		{
			++$line_num;
			$line = rtrim($line, "\n\r\0") . $this->line_separator;

			$parsed_line = preg_replace_callback('#^'. static::TIMESTAMP_REGEX .'<([^>]+)> ?(.*?)$#u', function($matches) use ($line_num) {
				return "<span class=\"timestamp d-none d-print-inline\" id=\"m{$line_num}\"><a href=\"#m{$line_num}\">{$matches[1]}</a></span>".
					'&lt;<span class="nick">'. htmlspecialchars($matches[2]) .'</span>&gt; '. htmlspecialchars($matches[3]);
			}, $line, 1);

			if ($parsed_line === $line)
			{
				$parsed_line = preg_replace_callback('#^'. static::TIMESTAMP_REGEX .'\* ([^ ]+) (.*?)$#u', function($matches) use ($line_num) {
					return "<span class=\"timestamp d-none d-print-inline\" id=\"m{$line_num}\"><a href=\"#m{$line_num}\">{$matches[1]}</a></span>".
						'* <span class="nick">'. htmlspecialchars($matches[2]) .'</span> '. htmlspecialchars($matches[3]);
				}, $line, 1);
			}

			if ($parsed_line === $line)
			{
				$parsed_line = htmlspecialchars($line);
			}

			$line = "<span class=\"ln\" id=\"l{$line_num}\"></span>{$parsed_line}";
			$content .= LinkifyText::convert($line, true);
		}

		fclose($fp);
		return $content;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getImplementation()
	{
		return $this;
	}
}
