<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Extension\LogViewer;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use SplFileInfo;

use DCBase\Handler\AbstractHandler;
use DCBase\WebRequest;
use DCBase\Response\ResponseHelper;
use DCBase\WebResponse;

use Twig\Environment as TwigEnvironment;
use DCBase\Twig\TextFormat\TextFormatExtension;
use DCBase\Twig\TextFormat\Parser\ParserInterface;
use DCBase\Common\StrUtil as str;

use DCBase\Extension\LogViewer\DCLogParser;

/** 
 * Handle displaying DC hub logs
 */
class LogHandler extends AbstractHandler
{
	/**
	 * Handle an incoming request for a resource outside of FrontController
	 * 
	 * @param  WebRequest       $request       The incoming request expected to be handled
	 * @param  string           $relative_path The relative file path as part of the query string
	 * @param  SplFileInfo|null $file          File object bound to a document root, referenced path may not exist and cannot be a symlink
	 * @return mixed                           Valid WebResponse instance to be served, any other values (e.g. false) will continue processing of the request in FrontController
	 */
	public function handleResource(WebRequest $request, string $relative_path, ?SplFileInfo $file = null) : ?WebResponse
	{
		if ($request->getPathSegment('filename') === 'devhub' && defined('DCLOG_DEVHUB_FILE') && is_string(DCLOG_DEVHUB_FILE))
		{
			// This has to reference a path within DCBASE_ROOT_PATH
			$file = new SplFileInfo(DCLOG_DEVHUB_FILE);
			if ($file->isFile() && $file->isReadable())
				return static::serveLogFile($request, $file, '@extension/devhub_log.html.twig',
					new DCLogParser(), $file->isLink());
		}

		// skip directories and non-existent files
		if (!$file->isFile() || !$file->isReadable())
			return null;

		return static::serveLogFile($request, $file, '@extension/dclog_view.html.twig', new DCLogParser());
	}

	protected static function serveLogFile(WebRequest $request, ?SplFileInfo $file, string $template_name,
		ParserInterface $parser, bool $allow_links = false) : ?WebResponse
	{
		try
		{
			// allow_links determines whether symlinks will be allowed by getTemplateName(...)
			$target_file_path = ResponseHelper::getTemplateName($request, $file, $allow_links);
		}
		catch (Exception $e)
		{
			return WebResponse::error($request, 403);
		}

		if ($request->variable('download', false) !== false)
			return WebResponse::file($request, $file, true);

		if ($request->variable('source', '') === 'raw')
			return WebResponse::text($request, $file, 'text/plain');

		$response = WebResponse::page($request, $template_name, static::processTemplateVars($request, $file, array(
			'log_file' => $target_file_path
		)));

		$response->extend(function (WebRequest $req, TwigEnvironment $env) use($parser) {
			// register the @extension namespace for Twig and add extra parser implementation
			$env->getLoader()->addPath(realpath(dirname(__FILE__)) . '/template/', 'extension');
			$env->getExtension(TextFormatExtension::class)->addParser($parser);
		});

		return $response;
	}

	protected static function processTemplateVars(WebRequest $request, ?SplFileInfo $file, array $vars) : array
	{
		if (!isset($vars['log_file']))
			return $vars;

		$matches = array();
		if (preg_match('#^files/meetings/(\d{4}-\d{2}-\d{2})/(?:[^:\*\?<>\|"\n]+)\.log#ui', $vars['log_file'], $matches))
		{
			$vars['log_name'] = ucwords(str_replace('_', ' ', str::strtolower($file->getBaseName('.log'))));
			$vars['log_day'] = strtotime($matches[1]);
		}

		return $vars;
	}
}
