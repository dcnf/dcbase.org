#### Site Theme and Template Files

This directory contains files for the base template in use on [dcbase.org](https://www.dcbase.org/).
The template uses basic [Bootstrap](https://getbootstrap.com/) (version 4) style, which is in fact
adapted from their old [narrow jumbotron](https://getbootstrap.com/docs/3.4/examples/jumbotron-narrow/)
example ([CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode)) for version 3.

Additionally the [Litera](https://bootswatch.com/litera/) theme from bootswatch.com is used as
the basis for the theme when pages are printed and users have the option to switch to a darker base
theme [Slate](https://bootswatch.com/slate/) also from bootswatch.com.

The organization's entire website, including the base [Twig](https://twig.symfony.com/) template
files with the supporting markup and utilities, is publically available over on 
[GitLab.com](https://gitlab.com/groups/dcnf) released collectively under the terms of the 
[GNU General Public License](https://www.dcbase.org/DCBase/LICENSE) (version 2), *unless otherwise
stated*.

For example the license does not automatically apply as is to the actual copy, that is the content
text not considered placeholder, hosted on the site using these templates or the html markup of said
templates produced as output (see license exception), and included inline within the template files,
which contains and refences work licensed under CC-BY 3.0 (see above).

Notably the organization's logo or other branding materials are not subject to the terms of either of
the mentioned licenses and may not be used except to represent the organization (addtitional terms
may apply, contact the organization for clarification on case by case basis at dcnf (at) dcbase.org).
