/**
* @package dcbase
* @copyright (c) 2019 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*/

function DCBase(base_url, theme_name, theme_fx) {
	this.theme = parseInt(Cookies.get('dcbase.theme_id') || -1);
	this.themes = ['default'];
	this.base_url = base_url;

	this.loadStyles();
	if (this.theme == -1)
		this.theme = this.themes.indexOf(theme_name);

	this.changeStyle(this.theme, true, theme_fx);

	this.initBackLinks();
	this.initPdfLinks();
	this.fixPrintPaging();

	this.cookieConsent({'link': $('a.cookie-link').attr('href')});
}

DCBase.prototype = {
	hasPdfSupport: function() {
		var ua = window.navigator.userAgent;
		return (typeof navigator.mimeTypes['application/pdf'] !== 'undefined') ||
		// Firefox 18+ will support PDFs inspite of potential absence of the mime-type
		((ua.indexOf("irefox") !== -1) && (parseInt(ua.split("rv:")[1].split(".")[0], 10) > 18));
	},

	regexEscape: function(str) {
		return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	},

	cookieConsent: function(options) {
		var self = this;
		var consent_cookie = parseInt(Cookies.get('dcbase.consent_cookie') || -1);

		if (consent_cookie != -1)
			return false;

		options = $.extend({
			'style': 'success',
			'title': 'Cookie Info:',
			'message': 'This site uses cookies to enhance user experience, by continuing to browse this site you agree to this.',
			'link': '/privacy',
			'link_text': 'Read More',
			'button_text': 'OK',
			'expire': 30
		}, options);

		if (options.link[0] === '/' && options.link[1] !== '/')
			options.link = self.base_url + options.link;

		$('body').append($(
			'<div class="alert alert-' + options.style + ' alert-dismissible fade show d-print-none cookie-consent" role="alert">' +
			'<i class="fas fa-info-circle fa-lg" title="' + options.title + '"></i> ' +
			options.message + ' <a class="alert-link" href="' + options.link + '">' + options.link_text + '</a>' +
			'<button type="button" class="btn btn-' + options.style + ' btn-sm float-right cookie-ok" data-dismiss="alert">' + options.button_text + '</button>' +
			'</div>'
		));

		$consent = $('div.cookie-consent button.cookie-ok');
		if ($consent.length != 1)
			return false;

		$consent.click(function() {
			Cookies.set('dcbase.consent_cookie', 1, {path: '/', expires: options.expire});
		});
	},

	loadStyles: function () {
		var self = this;

		$('link[rel*=style][title]').each(function() {
			if ($(this).attr('title') != 'default') {
				var name = $(this).attr('title');
				if (self.themes.indexOf(name) < 0)
					self.themes.push(name);
			}
		});
	},

	styleSelect: function ($selector) {
		var self = this;

		if (!$selector) {
			$selector = $('footer select.style-select');
			if ($selector.length != 1)
				return false;
		}

		self.themes.forEach(function (theme, i) {
			theme = theme.charAt(0).toUpperCase() + theme.slice(1);
			$selector.append($('<option>')
				.val(i)
				.text(theme));
		});

		$selector.change(function() {
			self.changeStyle(parseInt($(this).val()));
		});

		$selector.css({'z-index': '999', 'position': 'fixed', 'top': '5px', 'right': '5px', 'opacity': '0.5'})
			.val(self.theme)
			.removeClass('hidden');
	},

	styleToggle: function (styles, $link) {
		var self = this;

		if (!$link) {
			$link = $('footer a.style-toggle');
			if ($link.length != 1)
				return false;
		}

		var current_state = $.inArray(self.themes[self.theme], styles);
		if (styles.constructor !== Array || styles.length !== 2 || current_state === -1)
			return false;

		$link.removeClass('d-none')
			.text(styles[Math.abs(current_state - 1)].charAt(0).toUpperCase() + styles[Math.abs(current_state - 1)].slice(1));

		$link.click(function() {
			var theme_id = -1;
			if ($(this).text().toLowerCase() === styles[0]) {
				$(this).text(styles[1].charAt(0).toUpperCase() + styles[1].slice(1));
				theme_id = $.inArray(styles[0], self.themes);
			} else {
				$(this).text(styles[0].charAt(0).toUpperCase() + styles[0].slice(1));
				theme_id = $.inArray(styles[1], self.themes);
			}

			if (theme_id !== -1)
				self.changeStyle(theme_id);

			return false;
		});
	},

	changeStyle: function (theme_id, init, bg_fx) {
		var self = this;

		if ((theme_id == self.theme && !init) || theme_id < 0 || theme_id >= self.themes.length)
			return false;

		$('link[rel*=style][title]').each(function() {
			if ($(this).attr('title') != 'default')
				$(this).prop('disabled', true);

			if ($(this).attr('title') == self.themes[theme_id]) {
				$(this).prop('disabled', false);
				if (!bg_fx)
					bg_fx = $(this).data('effect');
			}
		});

		var changeClass = function (effect) {
			$('body').removeClass().addClass('bs-' + self.themes[theme_id]);
			if (effect)
				$('body').addClass('bg-effect ' + effect);
		};

		if (init) {
			$(function () { changeClass(bg_fx); });
		} else {
			changeClass(bg_fx);
		}

		Cookies.set('dcbase.theme_id', theme_id, {path: '/', expires: 365});
		self.theme = theme_id;
	},

	initBackLinks: function (selector) {
		var self = this;

		if (!selector)
			selector = 'a.back-link';

		$(selector).click(function() {
			var domain = window.location.hostname.split('.');
			if (domain.length > 2) domain.shift();

			domain = domain.join('.');
			if (document.referrer.indexOf(domain) != -1){
				parent.history.back();
				return false;
			}
		});
	},

	initPdfLinks: function (selector) {
		var self = this;

		if (!self.hasPdfSupport())
			return false;

		if (!selector)
			selector = '.content';

		$(selector + ' a[href^="' + self.regexEscape(self.base_url) + '"]').filter(function() {
			var href = $(this).attr('href').toLowerCase();
			var anchor_pos = href.lastIndexOf('#');

			// we don't care about links that already have query string provided
			if (href.slice(-4) === '.pdf' || (anchor_pos != -1 && href.slice(anchor_pos - 4, anchor_pos) === '.pdf')) {
				var anchor = (anchor_pos != -1) ? $(this).attr('href').slice(anchor_pos) : '';
				var link = (anchor_pos != -1) ? $(this).attr('href').slice(0, anchor_pos) : $(this).attr('href');

				$(this).attr('href', link + '?source=embed' + anchor);
			}
		});
	},

	printCallback: function(is_print, selector, wrap_in_table) {
		if (!selector)
			selector = '.row.marketing h2 + p, .row.marketing h3 + p, .row.marketing h4 + p';

		if (is_print) {
			$('link[rel*=style][title=paged]').prop('disabled', false);

			if (wrap_in_table) {
				$('main.content').removeClass('content').wrap('<table class="content print-table" />');
				$('div.print-spacer-header').removeClass('print-spacer-header').wrap('<thead class="print-spacer-header" />');
				$('div.print-spacer-footer').removeClass('print-spacer-footer').wrap('<tfoot class="print-spacer-footer" />');
				$('div#page-content').wrap('<tbody class="print-body" />');
			} else {
				$(selector).each(function() { $(this).add($(this).prev()).wrapAll('<div class="paging-fix" />'); });
			}
		} else {
			$('link[rel*=style][title=paged]').prop('disabled', true);

			if (wrap_in_table) {
				$('table.print-table > main').unwrap('.print-table').addClass('content');
				$('thead.print-spacer-header > div').unwrap('.print-spacer-header').addClass('print-spacer-header');
				$('tfoot.print-spacer-footer > div').unwrap('.print-spacer-footer').addClass('print-spacer-footer');
				$('tbody.print-body > div#page-content').unwrap('.print-body');
			} else {
				$(selector).each(function() { $(this).add($(this).prev()).unwrap('.paging-fix'); });
			}

			this.changeStyle(this.theme, true);
		}
	},

	fixPrintPaging: function (selector) {
		var self = this;
		var chrome = ("webkitLineBreak" in document.documentElement.style);

		if ('onbeforeprint' in window && 'onafterprint' in window) {
			window.onbeforeprint = function () { self.printCallback(true, selector, chrome); };
			window.onafterprint = function () { self.printCallback(false, selector, chrome); };
		} else if (typeof window.matchMedia !== 'undefined') {
			// no need to call manually on load, because printCallback(false) == noop
			window.matchMedia('print').addListener(function (event) {
				self.printCallback(event.matches, selector, chrome);
			});
		}
	}
}
