#### Donators {#donators}

DCNF can survive by member fees and donators' support. The following people have contributed money to the organization in some way, beyond member fees. Names are obfuscated slightly (if you do wish to remove yourself from the list, [let us know](https://www.dcbase.org/privacy#privacy)). Donation amounts are in Euros.

The member fees and the donations pay for two things: a webserver and the domain.

| Name/Organization		| Donation (€) | Date | {.table.table-striped}
| --------------------- | ------------ | -----------: |
| Andras T				|			3  | 2020-01-04
| Jerry J				|			5  | 2019-12-07
| Phil S				|			5  | 2019-10-04
| Ron R				|			10  | 2019-05-30
| David S				|			20  | 2019-05-05
| Damian P				|			5  | 2019-04-13
| Ringo R				|			15  | 2019-03-30
| Nigel S				|			8  | 2019-03-20
| EZHelp				|			20  | 2019-03-10
| Niklaus F				|			20  | 2019-02-24
| Transxcporp				|			26  | 2019-02-19
| Andras T				|			9  | 2019-01-16
| Marc B				|			5 | 2019-01-15
| Andrew T				|			20  | 2019-01-13
| Catalin G				|			5  | 2018-12-15
| Lianne M				|			10  | 2018-12-11
| Reliable Plumbing Peterborough				|			5  | 2018-12-11
| Ralph S				|			50  | 2018-11-28
| Stan T				|			10  | 2018-10-21
| Jose A				|			10  | 2018-08-29
| Alan D				|			10  | 2018-07-21
| Ron R				|			10  | 2018-06-04
| James B				|			20  | 2018-05-12
| Ringo R				|			10  | 2018-02-28
| Phil S				|			5  | 2018-01-08
| Michael W				|			5  | 2017-12-24
| poy					|			100 | 2017-12-24
| Michael C				|			10 | 2017-10-01
| Ralph S				|			40 | 2017-11-11
| Didier B				|			5  | 2017-11-10
| Ricardo A O			|			10 | 2017-10-30
| Nicolo D V			|			2  | 2017-10-28
| Dino R				|			10 | 2017-10-28
| Bevan H				|			25 | 2017-10-20
| Khalib R				|			5  | 2017-10-19
| Ansurag P				|			5  | 2017-10-08
| Peter D				|			3  | 2017-09-29
| Malcolm M				|			10 | 2017-09-24
| Heather A				|			10 | 2017-09-12
| Andrea S				|			1  | 2017-09-02
| John C				|			10 | 2017-08-23
| Sebastian A			|			10 | 2017-08-20
| Vladimir B			|			1  | 2017-07-19
| Geekazon.com			|			20 | 2017-06-18
| Petrica V				|			1  | 2017-06-03
| Valentin B			|			20 | 2017-05-13
| Trevor M				|			10 | 2017-05-07
| Jesse G				|			20 | 2017-05-06
| Veronika S			|			5  | 2017-03-25
| Robin L				|			1  | 2017-02-17
| Sven K				|			5  | 2017-01-06
| Conny L				|			30 | 2016-12-19
| A. J. G				|			5  | 2016-12-01
| Geoff J				|			5  | 2016-11-17
| Wilfried K			|			15 | 2016-11-13
| Bevan H				|			25 | 2016-10-31
| Vernon C				|			20 | 2016-10-22
| Janusz M				|			10 | 2016-10-08
| Didier B				|			10 | 2016-10-08
| Wilfried K			|			10 | 2016-10-07
| William W				|			5  | 2016-10-03
| Marios K				|			10 | 2016-09-24
| John L				|			50 | 2016-09-23
| Dries v D				|			5  | 2016-09-22
| Little Room Sales 	|			5  | 2016-09-22
| Alexander P			|			5  | 2016-09-20
| Donald C				|			20 | 2016-08-20
| Vincent P				|			3  | 2016-08-18
| Mark B				|			10 | 2016-08-08
| Michael V				|			5  | 2016-07-02
| Peter F				|			5  | 2016-06-26
| Borsos I				|			3  | 2016-05-15
| Tigran P				|			10 | 2016-01-20
| Yan V					|			20 | 2015-12-26
| Udris P				|			5  | 2015-10-25
| Zsolt K				|			10 | 2015-10-25
| Viktor M				|			2  | 2015-10-05
| Rob C					|			16 | 2015-09-23
| Borbas A				|			10 | 2015-09-22
| Ricardo J				|			40 | 2015-09-10
| Cristina S			|			15 | 2015-08-23
| Peter D				|			8  | 2015-08-21
| George H				|			10 | 2015-05-30
| Johan L				|			50 | 2015-05-28
| TNT Solutions			|			5  | 2015-05-24
| Ole J					|			15 | 2015-05-22
| MoonWillow's Realm	|			9  | 2015-05-10
| Uruc-Olaru			|			5  | 2015-05-09
| Steve H				|			5  | 2015-05-04
| Janusz M		        |			10 | 2015-04-29
| Valentin B			|			15 | 2015-04-21
| R P Horsley			|			23 | 2015-04-19
| Åke S					|			5  | 2015-04-17
| Patrick H				|			5  | 2015-04-14
| Alan D				|			5  | 2015-04-13
| eMTee					|			90 | 2015-02-14
