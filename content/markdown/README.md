#### Site markdown content

This directory contains files used by the template files on [dcbase.org](https://www.dcbase.org) to
pull page content or copy into the final resulting pages served to the user. Although they may also 
be viewed separately as "headless" versions of the documents.

The files use markdown but may be passed through [Twig](https://twig.symfony.com/) for preprocessing
before they are parsed. This is determined by the actual file extension, if the *real* file extension
is .twig the contents are first rendered using Twig before being parsed as markdown. Some template
files may also additionally contain inline markdown blocks as part of the file itself.
