### 3. Directory structure

In this section we will go over the various directories in the git tree and their roles and associated best practices
for content where relevant.

#### 3.1 System directories
These are covered here for the sake of completeness, and have little relevance to maintaining the content on the site
on day to day basis. They include the cache, config, public and vendor directories.

The cache directory contains various temporary and preprocessed files including but not limited to parsed markdown
blocks and files that have been pulled from the template or markdown files and git related metadata for display on the
pages.

The **config** directory, as its name suggests, contains various configuration files for the site. Most notable of these are
the mapping of known file extensions and several global constants. These config files are written in PHP and make use of
the special relationship between PHP's include and return statements to avoid global variables. In simple terms if an 
included file includes a return statement in global scope that file acts somewhat similar to a function body and the
result is passed through the include statement to the parent file and can be assigned to a variable not unlike a value
from a normal function call. The config files are annotated with PHP comments, should one need to edit them.

The **public** directory is the actual document root as far as the front facing web server is concerned, however, it should
only contain the index.php file that acts as the entry point for the site and backend. Any other files placed in this
directory will not be tracked through git or processed by the backend. It should be noted that since symbolic links are
not processed by the backend and they should be placed to the public directory on the hosting server if they are needed
for specific hosted content outside of this system.

The **vendor** directory has the same role as in PHP projects using Composer, however, Composer is not used in this instance
so that the git repository may function as a complete self contained backup of all dependencies required to run a copy of
the site at any point in time. This directory contains all the custom PHP classes for the backend, under the DCBase
namespace, as well as verbatim copies of Twig and other dependencies.

#### 3.2 Content directories
These cover the directories containing the actual files (templates) representing pages on the site as well as their
dependencies such as JavaScript and CSS. They include the content and template directories including any sub directories.

The **template** directory contains the base page template (bootstrap_base.html.twig) which pages on the site can, and
most should, inherit from to achieve a consistent look across pages. It should be noted that certain aspects of pages
covered in the following sections will rely on use of this base template. For example, if one wants to serve a page
without inheriting from the base template one has to either inherit from a modified version of the base template or
include the full HTML document structure in the pages main template file (the latter should be considered bad practice).

Along with the base template, this directory also contains Twig macro definitions and various stock templates such as 
error, redirect and directory indexing templates as well as the wrapper used for serving plain markdown files.

The **content** directory is what is best described as a the *virtual document root* of the website, anything placed here acts
as if served by the backed as if it was located in the document root of the site. While the structure of the content in
this directory is generally free form, for the sake of keeping things organized, several sub directories have been
created. Notably anything placed in the files sub directory of this directory is by default not under version control
unless explicitly versioned through gitignore. Additionally plain HTML files will not be served the default file format
is considered to be Twig template file (generally with the extension .html.twig, more details are in the following
section about content creation).

We will cover the virtual document root and its use more in the next section as we move to go over the topics of page
creation and editing.
