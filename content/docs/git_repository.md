### 2. The Git repository

The repository containing the website code is publicly available [here](https://gitlab.com/dcnf/dcbase.org), we ask
that you read the [LICENSE](https://www.dcbase.org/DCBase/LICENSE) and [README](https://www.dcbase.org/DCBase/README)
files prior to contributing or using any content in the repository.

#### 2.1 Issues and Merge Requests
Anyone, with a GitLab.com account, may create an issue for the project through GitLab.com issue tracker and these issues
will be reviewed by a project member and appropriate action will be taken after discussion.

In order to submit merge requests (also known as pull request) you must fork the project on GitLab.com under your
account namespace in order to create a branch and an accompanying merge request. Alternatively if you have at least
developer access level on the parent project you may work with merge requests directly on the parent project using
branches.

#### 2.2 Requesting write (developer) access
If you are directly affiliated with the Direct Connect Network Foundation as a member, and have successfully paid your 
membership fee for the year, you have the right to request direct write access to the organizations git repositories.
When making a request for repository access state your GitLab.com user name and the email on file with the organization,
to verify your member status, and a reason for your request.

You may also directly request access through the GitLab.com interface using the "Request Access" button on the
[project page](https://gitlab.com/dcnf/dcbase.org), however, when making a request like this it is important to know
that your request being accepted depends on whether your reason and membership status can be verified.

People who are not members of the organization, and affiliate projects who do not have member representation, may submit
a request for access through GitLab.com as above. However, in such cases the request being accepted depends on previous
contributions, including but not limited to the number of previously accepted merge requests or other contributions
such as patches, issues or other types of content submissions or other involvement with Direct Connect or the
organization.

Long term established members of the organization, or members of multiple of its projects, may have their GitLab.com
project membership moved to the group level for easier management.

#### 2.3 Content submissions
Content submissions are preferred in the form of merge requests or patches within issues, unless you have write access
to the project repository in which case you may make commits directly or use merge requests where appropriate. Before
submitting any content, make sure you have read and understood the entirety of this documentation. This is to save both
your and our time.

Most normal content submissions should only involve content within the virtual document root (see the section on content
directories) although any form of content and code submissions are welcome and will be reviewed by the relevant people.

#### 2.4 Automatic Deployment of changes
Changes are currently automatically deployed to the organization's public facing web server. This is achieved through the
use of Git webhooks, as commonly implemented by GitLab and GitHub, which the backend provides a trivial implementation for
that simply saves the payload to a file. The server then monitors this file and does a hard reset followed by git pull
on the public copy of this site whenever it detects a change.

Because all changes are generally immediately deployed the non-content files contained in the project repository are
generally locked to a single person who will manage their updates to avoid unintentionally breaking the public site
in such a way that does not get resolved in a timely fashion.

#### 2.5 Affiliate project's and their websites
Affiliate projects can have their own repositories that are served through the main installation of the DCBase backend
under a subdomain such as projectname.dcbase.org. The content in these repositories is generally subject to the same
rules and restrictions as the content in the main repository, with the notable exception that for these repositories
the repository root itself is the projects virtual document root.

The setup of the subdomain and the link between the repository and the deployment server requires a manual set up
process. However, after it is done, from the point of view of the affiliate project the link requires no maintenance
at all.

To request a subdomain and a repository to be set up or a repository to be linked to the system one should contact a
memeber of the organization's current board via email through dcnf (at) dcbase.org. The hosting location of the actual
repository is not mandated, however, it has to be able to support either GitLab or GitHub style webhooks system in
order for automatic deployment of changes to be possible. Asking a repository to be created, along with the required
subdomain, under the DCNF group on GitLab.com is always the easiest option but it is not a requirement.

Affiliate projects will have access to extend the default base style used on this site, however, they can also opt to
create their own custom Twig based template for their sub-site. Affiliate projects **will not** be able to deploy
custom PHP code through this repository.

For more information on Direct Connect Network Foundation and affiliate projects, please head over to the main site
[here](https://www.dcbase.org/affiliated_projects).
