### 4. Content creation

Content on the site is created mainly as Twig templates inheriting the base template that gives you the basic blocks and
all necessary boilerplate HTML in the final resulting page. While covering the entirety of Twig syntax in this document
is far outside the scope of it the syntax should be easy to pick up for anyone with previous experience with either
programming or templating systems used by various software making use of the MVC paradigm or its variants.

####  4.1 Twig syntax primer
Here we will quickly cover the basics of Twig syntax, however, this is not intended as a replacement of reading the Twig
[documentation](https://twig.symfony.com/doc/2.x/templates.html).

- Template variables or function calls intended to produce output on the page will be surrounded by ``{{`` and ``}}``.
- Block level tags, control flow and assignment statements (if, else, for, etc.) will be surrounded by ``{%`` and ``%}``.
- Comments will be surrounded by ``{#`` and ``#}``.
- Tag tokens can be printed as string literals, for example ``{{ '{{' }}`` prints the content of the literal as is on
the page. This is particularly relevant when passing markdown content through Twig (see below).
- Minus sign (-) following or preceding the above tokens inside the tag can be used for fine grained whitespace control.
- Single new line immediately following a tag will be removed from the final output.
- Block level tags and control flow statements will end with an end tag, for example ``{% endblock %}`` or ``{% endif %}``
- The ``{{ parent() )}`` statement will pull in content of the block it's used in from the parent template, without it
the content is replaced.
- Associative arrays and objects are accessed using the dot notation.
- Any file paths referenced in Twig templates are relative from Twig's template directories (the virtual document root
and the template directory specifically).
- String concatenation is done with the tilde (\~) character.

<div class="alert alert-info" role="alert" markdown="1">
<i class="fas fa-info-circle fa-lg" title="Info:"></i> 
The full Twig documentation, including complete reference of built in features, can also be found as a PDF over
[here](https://twig.symfony.com/pdf/2.x/Twig.pdf).
</div>

#### 4.2 Getting started
For an easy starting point it is recommended to have a look at the basic empty boilerplate document in the virtual
document root, under the ``docs/boilerplate`` directory, named
[blank_page.twig](https://www.dcbase.org/docs/boilerplate/blank_page.twig?source=view). This file is additionally
annotated with Twig comments and contains superfluous blocks and template directives for all available sections of the
base template.

If you want the resulting page to be served with the .html extension it should end in .html.twig. If you want to
primarily use markdown in your page the extension should be either .md or .md.twig (variants with .markdown are also
accepted). For an example of such page please consult
[meeting.md.twig](https://www.dcbase.org/docs/boilerplate/old/meeting_annual.md.twig?source=view). Note that in order to allow for
mixed Twig and markdown syntax, such as using any of the provided global template variables or macros, the extension
used has to be a variant ending in .twig. The inline use of markdown within a regular Twig template file will be
covered in a later section about the TextFormat extension provided by the backend.

#### 4.3 Naming pages and page lookup rules
When considering how to name a page as well as structuring the content in the virtual document root it is important to
know that the backend employs a simplistic fuzzy search based on the Request URI. For example if file foobar.html.twig
exists in the virtual document root it will be accessible either as /foobar.html or /foobar (if directory /foobar does
not exist). This is done to avoid creation of directories with just a single index file for the sake of the URL.

If Request URI does not contain a file name, the lookup appends /index to the query string when looking for the
file, if the requested file name extension is .html .twig will be appended to it. This means raw HTML files are not
served by the backend. As a general recommendation the names should be conservatively web safe and in particular spaces
should be avoided in directory and file names.

Some of these naming rules and guidelines are also covered briefly at the header of the blank page boilerplate file, if
you use the boilerplate file as a basis for your pages these notes should not be left in the final page template.

<div class="alert alert-info" role="alert">
<i class="fas fa-info-circle fa-lg" title="Info:"></i> 
Currently most valid paths in UTF-8 are accepted, with the exception of paths containing question marks. The backend
operates directly on the file system, however, the actual usable file names depend on the encoding the file names are
stored on the system.
</div>

#### 4.4 Support for directory indexes
If a directory exists but no suitable index file can be found with any extension directory index will be generated using
the stock template directory_index. Directory indexes generated by the backend have the ability to additionally display
a markdown or text README file from root of the directory being indexed. The lookup for the README is recursive and the
closest reachable readme will be displayed. When markdown is used it is important to use the correct extension for the
file (see above).

#### 4.5 Referencing local pages from Twig templates
This is related to the use of the ``make_url`` function, as it is documented below, it should always be used for URL's
relative to the address the site is hosted under (ie. local links). While from the current description of the function
it may seem trivial, however, this function exists so that local links can have special processing attached to them
if necessary. In other words the use of this function is for the sake of future proofing internal links.

This requirement also affects pages written in pure markdown, and as such markdown documents containing local links
should always use the .md.twig extension (or another equivalent variant) as outlined above
(see also: "Twig TextFormat extension" for info about quirks related to mixing Twig and Markdown syntax).

<div class="alert alert-info" role="alert" markdown="1">
<i class="fas fa-info-circle fa-lg" title="Info:"></i> 
For URL's with spaces in them, in order to produce valid HTML the use of ``util.sanitize_url`` wrapper is still always
required unless spaces are manually URL encoded to %20.
</div>
