### 1. Introduction

The DCBase.org website is currently managed through a git repository hosted at GitLab.com and served through a custom
PHP middleware layer (backend) written in PHP creatively named DCBase. Content is produced primarily using a combination
of Twig templates and Markdown or other Lightweight Markup Languages. This document consists of the primary end user
documentation for that backend system.

The problems when it came to deciding how the site should be managed were twofold, easy management without direct server
access and low maintenance overhead. Which roughly translates to doing a lot with very little actual backend logic. It
should be noted that in this case end user is generally equivalent to people with some level of software development
experience.

To allow the system to solve both of those problems the immediately obvious answer would have been to use a Static Site
Generator which the current system resembles a great deal. However, whether you use an SSG or not is a very binary
choice as using one generally excludes all possibility of any dynamic content on the server side (that is easily
integrated). While using an existing system created for managing primarily dynamic content, with generally a lower
barrier to entry, would have placed needless restrictions on the content structure.

So instead what this system does is go to the halfway point between static and dynamic. In short it is the modern
equivalent of Server Side Includes. In practice this means that its primary functionality is to act as a sort of
frontend for Twig and the file system of the server it's on. This is why arguably the most advanced piece of
functionality it has is actually packaged as a Twig extension separate from the thin layer of code that maps requests
to file system paths.

The majority of the code of the backend, Twig excluded, is actually about hiding the aged procedural ways that PHP handles
many aspects of the HTTP protocol. Of course all of this is technically work that has been done elsewhere, however, the
problem with most of those existing solutions is that they do too much at a level that is completely unnecessary for a
simple system like this. Because all a system like this really needs is enough to not make it look like it was created in
the late 20th century.
