#### Site boilerplate documents

This directory contains document templates to be used when creating new pages on the site, please
refer to the comments included within the templates, if any, for further details.
