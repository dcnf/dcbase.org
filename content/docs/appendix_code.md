### Appendix A: Code and Classes

This section contains a brief overview of the more technical aspects of the backend, including some epanded rationale
on why the system exists in the form it is in.

#### A1. DCBase Classes
This section contains a brief overview of the primary classes defined in the DCBase namespace, with short descriptions
of their role and purpose.

The classes and other code included in the TextFormat extension and the accompanying Parsedown class extension are sadly
outside the scope of this section. This is due to the fact that in order to understand the structure of both of these
components one has to have some level of knowledge about the internals of each of the related projects and covering that
in detail would require a lot more than what can reasonably be covered and maintained as part of this document. The
TextFormat extension to Twig in particular can essentially be considered its own subproject, albeit a very central one
in terms of easy content production, for the main DCBase system.

ClassLoader
:  This class provides the SPL autoloader implementation for the backend, it is mostly conformant with PSR-4 autoloader
specification by PHP-FIG. This component is something that has been inspired by existing autoloaders from other projects
and has transformed through several iterations since its initial conception.

ConFigLoader
:  This class handles loading PHP config files from within the config directory, it does so relying on the unique property
of PHP's include statement when the included file contains a return statement. This is done to avoid creating variables
in the global scope as much as possible.

DirectoryList
:  This class generates the template parameters array for the automatic directory indexing support of the backend from a
file path, it was initially created for a different project, also using Twig, and then ported forward to this one.

ErrorReporting
:  Set of functions, wrapped in a static class, whose purpose is to format PHP errors and exceptions into a more readable
format either as HTML or text (CLI).

FrontController
:  The entry point for the application, the FrontController::run method is basically the equivalent of the main() method
from many programming languages. Also implements the PHP error handlers that use the ErrorReporting functions and the end
points for Git Webhooks.

GitUtil
:  Collection of functions, wrapped in a static class, that interface with Git like the logic behind the webhooks
implementation and the actual final implementations for the ``git_info`` template function exposed by the backend to Twig.

WebRequest
:  This class, along with its response counterpart is responsible for abstracting away the age old super globals and the
procedural ways PHP deals with HTTP protocol in general. Notably this class, particularly its public interface,
has been inspired by work originally done by Nils Aderman for the phpBB project, further details in the header of the
file.

WebResponse
:  This is the response counterpart for the above WebRequest class. Its role is to act both as a base class and a
container for factory methods of several typed response classes, among them most notably File, Text and Twig responses.

Common\StrUtil
:  This static class contains a partial set of wrappers for PHP's multibyte string functions, for better UTF-8 support,
as well as what could be called serviceable implementations for a few missing ones.

Common\Util
:  Procedural style functions that really have no better place to reside in, includes for example a set of wholly
unscientific functions for coarse measuring of execution time and memory usage of the PHP script. As well as function
for formatting dates as string relative to a point in time (implementation for ``date_relative`` template function).

Response\FileResponse
:  This typed response represents any real file on the servers file system, served as is, includes support for NGINX
specific ``X-Accel-Redirect`` server side header. Also contains theoretical support for partial downloads, however, this
has only been partially tested due to git being unsuitable for large binary files to begin with. Support for HTTP caching
headers.

Response\TextResponse
:  This typed response represents any text based file or generated string response, supports HTTP cache headers, for
actual files, and variable mime-types.

Response\TwigResponse
:  This typed response represents anything passed through Twig, creating and managing the twig environment, also contains
several auxiliary methods for building up the template parameter array. Contains implementations or wrappers to
implementations for most custom template functions.

Response\JsonResponse
:  This typed response represents, usually dynamically generated, JSON response. Includes support for JSON with padding
for the scenarios where it is still necessary. Does not support any HTTP cache mechanisms. Suited for API responses.

Response\XmlResponse
:  This typed response represents, usually dynamically generated, XML response supports both ``SimpleXml`` and
``DOMDocument`` out of the box, along with strings and files, however, ill-suited for static files due to lack of
support for caching. Likewise suited for API responses.

#### A2. DCBase: Code, Usage and Design
The nucleus of the system is really formed around the WebRequest and WebResponse pair of classes, obviously including the
various typed responses. That and the custom Twig environment.

Generally speaking the code is using a basic Front Controller pattern, as the name of the entry point class would suggets,
where every control path of its main method ``FrontController::run(WebRequest $request)`` should end in return statement
that calls the ``send()`` method of an instance of ``WebResponse`` which matches the parameters of ``$request``.

As for the WebResponse instances, their public interface aims to be such that most of the time all that is required is a
call to one of the static factory methods of the abstract parent class corresponding to the expected return type, with the
correct parameters, chained directly to a send call. The best code usage examples, would actually probably be the myriad of
factories defined as part of WebResponse. Particularly the ones for errors and redirects.

Additionally, the system aims deliberately to be as stateless as possible, excluding data that is the same for every visiting
user when it comes to what happens on the server side the default style add some limited state through the use of cookies.
Which ironically is in part to comply with the largely useless EU directive about browser cookies.

Drawing some parallels to the ever popular MVC pattern the Model doesn't really exist, View would correspond to the Twig
template which most content pages inherit from and the only controller would be the aforementioned front controller, because
there really is only one action the backend does and that is map a file (or a set of files) to a request URI. The closest thing
to a model, that being the actual content of the page, would be then be the Twig template file inheriting the base template. The
generic views for PDF's, page source, and markdown make the comparison a little more obvious.

In this system the Twig templates itself are used as a both a content authoring format and tool as well as the way to present the
content. Resulting in no hard boundaries existing between content and presentation but rather creating a soft boundary, through
template inheritance, that the user can choose to ignore respect this boundary or not.
