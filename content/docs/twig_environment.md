### 5. Twig Environment

In this section we will cover the custom functions exposed to Twig templates by the backend as well as expanded syntax and
predefined macros and global variables.

#### 5.1 Custom functions and filters
The backend exposes a handful of custom Twig functions and filters for templates to use. On more about Twig functions
see the [documentation](https://twig.symfony.com/doc/2.x/templates.html#functions) and Twig's list of built in functions.

Unlike functions filters are applied to template variables using the pipe (|) character, so for example
``{{ timestamp | date_relative(now) }}`` would apply the below relative_date filter to template variable timestamp with
now as the optional argument. Multiple filters may be applied to a single variable by repeating the syntax. On more
advanced use of filters see the Twig [documentation](https://twig.symfony.com/doc/2.x/templates.html#filters) and Twig's
list of built-in filters.


    date_relative([now = null])

**Filter** that returns relative date representation from a time stamp and a fixed point in time (the argument now, which
by default is the current time). The time can be either a unix time stamp or a standardized string representation
compatible with ``strtotime``. Example result: "3 weeks ago" or "5 minutes to go".


    git_info(files[, author_info = true])

**Function** that returns an associative array of git file information such as the last time the file was changed, the
commit it was changed in as well as by default information about author of the commit, including email. If multiple
files are provided as an array the information about most recent of these files is returned. The intended use scenario
is ``{% set git_info = git_info([_self, ...]) %}`` where ``_self`` represents the file the call is made from. If the
template  file includes any external files then those should be provided in the array as paths relative to the virtual
document root or template directory.


    load_json(file)

**Function** that loads a JSON file as an associative array and returns it. This should be used in conjunction with the
set template template directory in the same way as the previous ``git_info`` function in order to be useful. If the file
can not be found or accessed this function returns an empty value. Internally the PHP function ``json_decode`` is used.


    pathinfo(option)

**Filter** that exposes the PHP function ``pathinfo`` for use within templates. The option argument should be the name of
one of the options supported by the underlying PHP function as a string or a combination of the options as an integer.
The other semantics of this filter are identical to the original PHP function.


    make_url(resource[, params])

**Function** that turns relative URI into an absolute one, and appends a query string (params) if given. Generally the
preferred way get the URL for local links over hard coding relative or absolute URL's or using string concatenation.
The best practice is to pass all links through ``make_url`` unless you are referencing the current page or the domain
root in which case use of the globals covered below is preferred.


    is_debug, page_time([format = true]), page_memory([format = true])

This set of **functions** is for very coarse and unscientific performance profiling and fun statistics.
- is_debug: returns true depending on whether the global constant DEBUG is defined or not, roughly equivalent to calling
the built in Twig function constant like this ``constant('DEBUG')``.
- page_time: returns page execution time up to the function call either formatted as a string or as a float.
- page_memory: returns page memory usage up to the function call either formatted as a string or as bytes.

#### 5.2 Global template variables
In addition to the global template variables defined by Twig the following variables are always set for all templates.

- content_encoding: the encoding of the current page, usually the literal "utf-8".
- base_url: the URL the backend is served under, including protocol and port, no trailing slash.
- page_url: the full URL for the current page that corresponds to the template file.

Adittionally a few global variables are set from templates using the set template directive.

- theme: information about the template as an associative array, see theme.json in the template directory.
- content_owner: translates to the literal "Direct Connect Network Foundation" (set by bootstrap_base)
- hide_menu: should be set to true if a template wants to not have the top level menu present on the resulting page.
- page_subtitle: should be a short, preferably one word, title describing the page may be used in the top level menu.
**must** be set by each template inheriting the base template.
- git_info: should be set to cache the result of the git_info template function.
**must** be set by each template inheriting the base template, may be set by other templates and **must not** be set by
template files not under version control (including files matching .gitignore rules).

#### 5.3 Predefined macros
The backend comes with a set of predefined macros, they are defined in the macros.twig file in the template directory
and imported under the name util by the base template accessing these macros is done using the dot notation, ie.
``{{ util.format_bytes(size) }}``. The main goal of these macros is to reduce repetitive markup or do some basic
tasks such as in the previous example formatting template variable with a byte size as a human readable.

The macros include menu helpers that make the base templates menu markup generally more readable as well as
``sanitize_url`` wrapper for ``make_url`` to address inconsistent escaping issues caused by URL's that predate this
system. The rest of the macros in this file are mostly trivial and self explanatory, looking at the file itself is
advisable before using any of these macros. The use of the aforementioned ``sanitize_url`` macro should be avoided in
the future (see caveat above).
