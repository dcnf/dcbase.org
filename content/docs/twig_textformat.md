### 6. TextFormat Extension (Twig)

Twig understandably does not provide support for any lightweight markup languages (LML) out the box. So to address
this for our use case of Twig the backend comes with a simple Twig extension that allows extending the core set of
Twig tags and filters with ones for third party markup parsers written in PHP.

The extension adds both block level tags, for inline content, as well as include style tags for all supported parsers.
Both the block level and include style tags support caching of the parsed content using flat files, with an optional
hint to disable caching on demand.

Twig filters are also registered, however, caching is not supported for these filters so their use is not recommended
outside short trivial string literals or variables containing such content.

Note that throughout the rest of this section markdown is used as a general example, but everything below applies for
every supported lightweight markup language. In all of the following examples the tag name (ie. markdown) corresponds to
the parser being used and can be replaced with the lowercased name of any supported parser.

#### 6.1 Block style tags
The syntax for block style tags for including formatted content inline inside a Twig template file is outlined below.
Here the extension specific additional literal token ``nocache`` controls whether the parsed content of the block is cached
on the file system or not (caching always happens by default).

Optionally, in place of the nocache token, the tag can include a ``depends on`` declaration which allows for fine grained
control of when the cache corresponding to this tag will be invalidated, which by default happens only when the file containing
the tag has changed. Including the depends on declaration ammends this condition to include the file or files that are
referenced by it.

    {% markdown[ nocache|depends on 'path/to/another/file.ext'] %}
    Markdown formatted content here.
    {% endmarkdown %}

#### 6.2 Include style tags
The syntax for including a file from within a Twig template file is shown below. Note that this tag is an extension of the
standard Twig ``include`` tag so its arguments can also optionally include the same arguments as the standard tag (below
represented collectively as ``twig_opts...``). For details on the nocache and depends on arguments of this tag see the
previous section on the block style tag as these behave identically for both variants.

However, pay attention to the order in relation to any arguments inherited from the Twig include tag as the extension
specific arguments must precede these other arguments, also, any twig specific arguments will be *silently ignored* if
the included file is not processed by Twig (see below).

    {% markdown 'path/to/file.md'[ nocache|depends on 'path/to/another/file.ext' twig_opts...] %}

If the file name above ends in .twig, such as .md.twig in the case of markdown, as outlined in earlier sections the file
contents are first processed by Twig before being passed on to the respective parser implementation. Using the nocache hint
for files that get processed by Twig before being rendered by this extension is usually advised so that incorrect cached
information isn't displayed to the user if request specific dynamic Twig variables are used.

<div class="alert alert-warning" role="alert" markdown="1">
<i class="fas fa-exclamation-circle fa-lg" title="Performance:"></i> 
If you need to include markdown file from within a markdown block, or another markdown file with .twig extension, always use
the standard Twig ``include`` tag within the file instead of the above tag. The markdown from the included file will be
parsed, **and cached**, with its parent.
</div>

#### 6.3 The pitfalls of mixed syntax scenarios
When external markup, parsed by the third party parsers, and Twig syntax is mixed within a block, or a file ending in
.twig, it should be noted that sometimes the syntax overlaps with Twig reserved patterns and in such cases outputting
these parts as string literals using Twig syntax is required. In the case of the below example, using markdown extra
(as implemented by the underlying parser Parsedown), the recommended syntax to avoid conflicts with Twig in the case of
the anchor syntax for headings is as follows.

    # Markdown heading {{ '{#anchor}' }} 

Note that, either a single space, as above, or an additional newline must follow the Twig output syntax, in this case,
as otherwise there might not be a new line break to end the heading (see Twig syntax rules outlined in an earlier
section) in the output to be parsed as markdown by the extension.

#### 6.4 The ``metafile`` tag and frontmatter
Using LML's with the backend in this fashion is all well and good, however, in order to customise the Twig template
including or wrapping such markup in any way one would have to create a wrapper template file to do so. In order to
decrease the cases where individually customised wrappers are required for trivial changes, to the base template, the
TextFormat extension registers an additional Twig tag ``metafile`` with syntax that is nearly identical to the include
style tags presented above except that this tag does not accept any parameters apart from the name of the file.

    {% metafile 'path/to/file.md' %}

Placing the above tag into a Twig template will extract what is commonly known as "frontmatter" from the referenced
file and and inject all the attributes from this data into the Twig template context, which is equivalent to using the
Twig ``set`` directive to set the values inside a template file. The frontmatter has to be specified at the top of the
file using one of the below syntaxes in order to be recognized.

	HTML style comments (useful for files without .twig extension)
    <!-- metadata: ... -->

    Twig style comments
    {# metadata: ... #}

The data placed in between these tags should be a well formed JSON object, with named key and value pairs (ie. you can
consider it to be an associative array or a map object).

By using the ``metafile`` tag it is possible for the backend using the TextFormat extension to use a generic view for
serving all files using a particular LML's while still retaining the ability to allow some customization by using the
frontmatter syntax from the file included in the view.

<div class="alert alert-info" role="alert" markdown="1">
<i class="fas fa-info-circle fa-lg" title="Info:"></i> 
Example of proper use of ``metafile`` to build a generic view, by the DCBase backend, would be the template file
``templates/markdown_view.html.twig`` used for serving any markdown file accessed directly from the web.
</div>

#### 6.5 Adding new parsers
In order to add support for additional parsers for this extension one must implement the simple ``ParserInterface``
interface which is declared under the ``DCBase\Twig\TextFormat\Parser`` namespace. It is recommended that
all implementations also be placed in this namespace.

Requirements for parsers to use are only that it can format and produce document fragments. This is because a parser
only capable of handling or parsing full documents can not be reliably used to parse the kind of content suitable for
embedding into the HTML output of the final pages. Obviously the parser also has to be either implemented in or callable
from PHP, however, for parser implementations that execute system commands both security and speed, of the call outside
of PHP, should be a primary concern.

As of right now the background provides a single parser for including and parsing markdown using the implementation
of markdown by Emanuil Rusev, [Parsedown](http://parsedown.org/) and its accompanying extension ParsedownExtra. The
original classes are slightly augmented by a trivial extension to its table syntax using a trait defined under the
namespace ``DCBase\Parsedown`` aliases for the original Parsedown classes importing this trait are also provided.
This extra complexity is to provide better support for tables styled by [Bootstrap](https//getbootstrap.org), that
the default style on this site uses, by expanding Parsedowns attribute syntaxt to the column definition portion of
the table markup.
