#### Site static content directory

This directory can be used to house site content in the form of independent files that may not be suitable for git version
control. The same rules apply to this directry as any other directory served through dcbase.org, the implied difference
is in how this files are handled through git, see .gitignore rules in the root of the repository for specific details.

Most files under this directory should fall under the ignore rules, with the notable exception of files such as meeting
logs and documents that are plain text or markdown files. Difference to using this directory as opposed to the /public/
directory that acts as the document root is that directory indexing generated with the site template is supported here
in addition to other features such as markdown and README processing.
