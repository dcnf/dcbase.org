# Parsedown and ParsedownExtra

This folder contains verbatim copies of Parsedown and ParsedownExtra by Emanuil Rusev (versions 1.8.0-beta-7 and 0.8.0-beta-1 respectively. These classes are licensed under the MIT license, see the acompanying LICENSE file.

These are the last versions that use the original single class structure, and contain fixes ahead of a major 2.0 rewrite. However, apparently 1.8 will never be released as "stable".

