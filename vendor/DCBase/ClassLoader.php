<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;

use DCBase\Common\StrUtil as str;

class ClassLoader
{
	// Store path info
	protected $locations = array();
	protected $class_map = array();

	// Hold the registration state of the class loader
	protected $registered = false;

	/**
	 * Create the class loader, with optional static class mapping
	 */
	public static function create(array $class_map = array(), bool $register = false) : ClassLoader
	{
		return new static($class_map, $register);
	}
	
	protected function __construct(array $class_map, bool $register)
	{
		$this->class_map = array_merge(array(
			// static class map for *all* dependencies for this class
			'DCBase\Common\StrUtil'	=> DCBASE_VENDOR_PATH . 'DCBase/Common/StrUtil.' . PHP_EXT
		), $class_map);

		if ($register)
			$this->register();
	}

	public function __destruct()
	{
		if ($this->registered)
			$this->unregister();
	}

	/**
	 * Installs this class loader on the SPL autoload stack.
	 */
	public function register(bool $prepend = false) : void
	{
		spl_autoload_register(array($this, 'loadClass'), false, $prepend);
		$this->registerd = true;
	}

	/**
	 * Uninstalls this class loader from the SPL autoloader stack.
	 */
	public function unregister() : void
	{
		spl_autoload_unregister(array($this, 'loadClass'));
		$this->registered = false;
	}

	/**
	 * Is the class loader registered with the spl
	 */
	public function isRegistered() : bool
	{
		return $this->registered;
	}

	/**
	 * Set an array of static class name to file path mappings
	 */
	 public function setClassMap(array $class_map) : void
	 {
		$this->class_map = $class_map;
	 }

	/**
	 * Add standard application paths
	 */
	public function addDefaultPaths() : ClassLoader
	{
		$this->addPaths(DCBASE_VENDOR_PATH);
		return $this;
	}

	/**
	 * Add location for the autoloader to look through
	 */
	public function addPaths($paths, ?string $prefix = null, ?string $suffix = null) : ClassLoader
	{
		$path_key = $prefix . '.' . $suffix;
		if (empty($prefix) && empty($suffix))
		{
			foreach ((array) $paths as $path)
				$this->locations[] = $path;
		}
		else if (isset($this->locations[$path_key]))
		{
			$this->locations[$path_key]['paths'] = array_merge(
				$this->locations[$path_key]['paths'],
				(array) $paths
			);
		}
		else
		{
			$this->locations[$path_key] = array(
				'prefix' => $prefix,
				'suffix' => $suffix,
				'paths' => $paths
			);
		}

		return $this;
	}

	/**
	 * Calls findFile() and loads the class file
	 */
	public function loadClass(string $class) : void
	{
		// check mapped classes
		if (isset($this->class_map[$class]) && is_file($this->class_map[$class]))
		{
			require($this->class_map[$class]);
		}
		else if (!empty($this->locations))
		{
			$file = $this->findFile($class);
			if ($file !== null)
				require($file);
		}
	}

	/**
	 * Find the expected class name
	 */
	protected function findFile(string $class_name) : ?string
	{
		// prepare the set of lookup paths
		$lookup_paths = array();
		$class_file =  null;

		foreach ($this->locations as $location)
		{
			if (is_array($location))
			{
				$prefix = $location['prefix'];
				$suffix = $location['suffix'];
				$name_clean = null;

				if (!empty($prefix))
				{
					if (str::strpos($class_name, $prefix) !== 0)
						continue;

					$name_clean = str::substr($class_name, str::strlen($prefix) + 1);

					if (!empty($suffix))
					{
						$suffix_len = str::strlen($suffix);
						if (str::strpos($class_name, $suffix, str::strlen($class_name) - $suffix_len) === false)
							continue;

						$name_clean = str::substr($name_clean, 0, str::strlen($name_clean) - ($suffix_len + 1));
					}
				}
				else if (!empty($suffix))
				{
					$suffix_len = str::strlen($suffix);
					if (str::strpos($class_name, $suffix, str::strlen($class_name) - $suffix_len) === false)
						continue;
	
					$name_clean = str::substr($class_name, 0, str::strlen($class_name) - ($suffix_len + 1));
				}

				if ($name_clean === null)
					continue;

				if (!$class_file || str::strlen($name_clean) < str::strlen($class_file))
					$class_file = strtr($name_clean, '_\\', '//');

				// make sure the more specific paths have precedence
				$lookup_paths = array_merge((array) $location['paths'], $lookup_paths);
			}
			else
			{
				$lookup_paths[] = $location;
			}
		}

		// we don't have a file name yet, class name did not match anything special
		if (!$class_file)
			$class_file = strtr($class_name, '_\\', '//');

		// look through the paths that match the initial class name
		foreach ($lookup_paths as $dir)
		{
			$class_path = $dir . $class_file . '.' . PHP_EXT;
			if (is_file($class_path))
				return $class_path;
		}

		// ... still here? give up.
		return null;
	}
}
