<?php
/**
*
* @package dcbase-common
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Common;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

use DCBase\Common\Util;
use DCBase\Common\StrUtil as str;

/**
 * Static class for git related functions
 * note: no IN_DCBASE check since this has no dependency on DCBASE_* constants.
 */
class GitUtil
{
	const REF_MASTER = 'refs/heads/master';

	// private constructor so no instances can be created
	final private function __construct() { }

	/**
	 * Lazy check for whether the source is under version control by git
	 */
	public static function isGit(string $repository_root, ?string $file = null) : bool
	{
		return (is_dir($repository_root . '.git/') && is_readable($repository_root . '.git/'))
			&& ($file === null || str::strncmp($repository_root, $file, str::strlen($repository_root)) == 0);
	}

	/**
	 * Get git information for a file, I don't like running system commands but the info has to come from somewhere
	 * @todo look into more intelligent cache solutions...
	 */
	public static function getLocalFileInfo(string $file, ?string $repository_root = null) : ?array
	{
		if (!defined('GIT_BIN_PATH') || !is_executable(GIT_BIN_PATH) || !function_exists('exec'))
		{
			trigger_error('GitUtil: undefined git constants or missing required functions.', E_USER_WARNING);
			return null;
		}

		if ($repository_root === null)
		{
			$repository_root = (defined('GIT_REPOSITORY_PATH')) ? GIT_REPOSITORY_PATH : null;
			if ($repository_root === null)
			{
				trigger_error('GitUtil: unable to set any repository root.', E_USER_WARNING);
				return null;
			}
		}

		$file_checked = str_replace(array("\0", ';'), '', $file);
		if ($file !== $file_checked || !is_file($file) || !static::isGit($repository_root, $file))
		{
			trigger_error('GitUtil: unable to resolve requested file path.', E_USER_WARNING);
			return null;
		}

		$cache_file = 'fileinfo/' . sha1($file) . '.json';
		$result = null;

		if (Util::isDebug() || ($result = static::getGitCache($cache_file, true, filemtime($file))) === null)
		{
			// for nested repositories and submodules
			$working_dir = getcwd();
			$changed_dir = ($working_dir !== false && @chdir(dirname($file)));
			$git_binary = escapeshellcmd(GIT_BIN_PATH);

			if (!$changed_dir)
				return null;

			// arguments
			$file_path = addslashes(str_replace(array($repository_root, '%'), array('/','%%'), $file));
			$file_arg = escapeshellarg(addslashes(basename($file)));
			$git_format = '{%n "commit": "%H",%n "author_name": "%an",%n "author_email": "%ae",%n "date": "%ad",%n "timestamp": %at,%n "message": "%f",%n "file": "'.
				$file_path .'" %n}';

			$git_commands = array();
			$git_commands[] = "{$git_binary} status --porcelain --ignored {$file_arg} 2>&1";
			$git_commands[] = "{$git_binary} log --max-count=1 --date=rfc2822 --pretty=format:'{$git_format}' {$file_arg} 2>&1";

			$result = array();
			$exit_code = 0;

			exec(implode(' && ', $git_commands), $result, $exit_code);

			chdir($working_dir);

			if ($exit_code === 0)
			{
				$status = array();
				while (!empty($result) && $result[0] !== '{')
					$status[] = array_shift($result);

				$result = (array) json_decode(implode(PHP_EOL, $result), true);
				if (!empty($status))
				{
					$result['file_status'] = str::substr($status[0], 0, 2);
					if ($result['file_status'] === '!!' || $result['file_status'] === '??')
					{
						$result['timestamp'] = filemtime($file);
						$result['date'] = date('c', $result['timestamp']);
					}
				}
			}
			else
			{
				$result = array('exit_code' => $exit_code, 'error' => implode(PHP_EOL, $result));
			}

			if (!Util::isDebug() && !empty($result))
				static::putGitCache($cache_file, $result);
		}

		return $result;
	}

	/**
	 * Simple file based caching for git data
	 */
	public static function getGitCache(string $file_relative, bool $return_array = true, ?int $source_time = null)
	{
		$result = null;
		$file_relative = ltrim($file_relative, '/');

		if (!defined('GIT_CACHE_PATH') || !is_dir(GIT_CACHE_PATH) || !is_writable(GIT_CACHE_PATH))
			return $result;

		$cache_file = GIT_CACHE_PATH . $file_relative;
		if (is_file($cache_file) && static::isCacheFresh($source_time, filemtime($cache_file)))
			$result = json_decode(file_get_contents($cache_file), $return_array);

		return $result;
	}

	protected static function isCacheFresh(int $source_time, ?int $cache_time) : bool
	{
		// invalidate cache on changes to this file
		if(filemtime(__FILE__) >= $cache_time)
			return false;

		return ($source_time == null || $cache_time >= $source_time);
	}

	public static function putGitCache(string $file_relative, $contents) : ?int
	{
		if (!defined('GIT_CACHE_PATH') || !is_dir(GIT_CACHE_PATH) || !is_writable(GIT_CACHE_PATH))
			return null;

		$file_relative = ltrim($file_relative, '/');
		$cache_file = GIT_CACHE_PATH . $file_relative;

		if (is_object($contents) || is_array($contents))
			$contents = json_encode($contents);

		Util::ensureDirectory(dirname($cache_file));
		return Util::stringToFile($cache_file, $contents);
	}

	public static function pruneGitCache(int $stale_time = 3600 * 96) : bool
	{
		if (!defined('GIT_CACHE_PATH') || !is_dir(GIT_CACHE_PATH) || !is_writable(GIT_CACHE_PATH))
			return false;

		$stale_time = time() - $stale_time;

		foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator(GIT_CACHE_PATH), RecursiveIteratorIterator::LEAVES_ONLY) as $file)
		{
			if ($file->isFile() && $file->getATime() < $stale_time)
				@unlink($file->getPathname());
		}

		return true;
	}
}
