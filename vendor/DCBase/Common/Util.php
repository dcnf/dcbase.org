<?php
/**
*
* @package dcbase-common
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Common;

use Exception;

/**
 * Static class for common auxiliary functions
 * note: no IN_DCBASE check, because this is also used by TextFormat Twig extension (also has no dependency on DCBASE_* constants)
 */
class Util
{
	// private constructor so no instances can be created
	final private function __construct() { }

	/**
	 * Checks if we are running from command line
	 */
	public static function isCLI() : bool
	{
		return (PHP_SAPI === 'cli' || (substr(PHP_SAPI, 0, 3) === 'cgi' && @$_SERVER['REMOTE_ADDR'] === ''));
	}

	/**
	 * Checks if we are running in dev environment
	 */
	public static function isDebug() : bool
	{
		return defined('DEBUG') && DEBUG;
	}

	/**
	 * Make sure directory exists on file system
	 */
	public static function ensureDirectory(string $dir_path, int $permissions = 0755) : bool
	{
		return !is_dir($dir_path) && mkdir($dir_path, $permissions, true);
	}

	/**
	 * file_put_contents replacement that isn't subject to implementation quirks of the original
	 */
	public static function stringToFile($file_path, $string, $file_mode = 'w') : ?int
	{
		$fp = fopen($file_path, $file_mode);
		if ($fp === false)
			return null;

		$result = fwrite($fp, $string);
		fclose($fp);

		return $result !== false ? $result : null;
	}

	/**
	 * Simple relative date function
	 */
	public static function getRelativeDate($time, $now = null) : string
	{
		if (is_null($now) || $now === 'now')
			$now = time();

		if (is_string($now))
			$now = ctype_digit($now) ? (int) $now : strtotime($now);

		if (is_string($time))
			$time = ctype_digit($time) ? (int) $time : strtotime($time);

		$elapsed_time = abs($now - $time);
		if ($elapsed_time < 1)
			return 'just now';

		$tail = $time > $now ? ' to go' : ' ago';

		$unit_map = array(12 * 30 * 24 * 60 * 60	=> 'year',
						  30 * 24 * 60 * 60			=> 'month',
						  07 * 24 * 60 * 60			=> 'week',
						  24 * 60 * 60				=> 'day',
						  60 * 60					=> 'hour',
						  60						=> 'minute',
						  01						=> 'second');

		foreach ($unit_map as $seconds => $name)
		{
			$units = $elapsed_time / $seconds;
			if ($units >= 1)
			{
				$units = round($units);
				return $units . ' ' . $name . ($units > 1 ? 's' . $tail : $tail);
			}
		}
	}

	/**
	 * Execution statistics
	 */
	public static function currentScriptTime($start_time, $format)
	{
		// return time used for response generation
		$current_time = microtime(true) - $start_time;
		return $format ? sprintf("%.3f", $current_time) : $current_time;
	}

	public static function currentScriptMemory($base_usage, $format)
	{
		$current_memory = memory_get_usage();
		$current_memory -= $base_usage;

		if ($format)
		{
			$current_memory = ($current_memory >= 1048576) ? round((round($current_memory / 1048576 * 100) / 100), 2) . ' MiB' : 
				(($current_memory >= 1024) ? round((round($current_memory / 1024 * 100) / 100), 2) . ' KiB' : $current_memory . ' Bytes');
		}

		// return memory used relative tó base_usage
		return $current_memory;
	}

	/**
	 * Check if array has associative or sequential keys, note: only 0 indexed and empty arrays are treated as not associative
	 */
	public static function isAssocArray(array $arr) : bool
	{
    	if (array() === $arr)
    		return false;

    	return !array_key_exists(0, $arr) || (array_keys($arr) !== range(0, count($arr) - 1));
	}
}
