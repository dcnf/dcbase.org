<?php
/**
*
* @package dcbase-common
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Common;

use Exception;
use Normalizer;

/**
 * Helper functions and wrappers for multibyte string functions (incomplete)
 * note: no IN_DCBASE check, because this is also used by TextFormat Twig extension (also has no dependency on DCBASE_* constants)
 */
class StrUtil
{
	const ENCODING_INTERNAL = 'ISO-8859-1';
	const ENCODING_UTF8 = 'UTF-8';

	// private constructor so no instances can be created
	final private function __construct() { }

	/**
	 * Some missing multibyte string functions
	 */
	public static function strnatcmp(string $str1, string $str2) : int
	{
		if ($str1 === $str2)
			return 0;

		$str1 = preg_replace('/\p{Mn}+/u', '', Normalizer::normalize($str1, Normalizer::NFD));
		$str2 = preg_replace('/\p{Mn}+/u', '', Normalizer::normalize($str2, Normalizer::NFD));

		return strnatcmp($str1, $str2);
	}

	public static function strcmp(string $str1, string $str2) : int
	{
		if ($str1 === $str2)
			return 0;

		$str1 = Normalizer::normalize($str1, Normalizer::NFD);
		$str2 = Normalizer::normalize($str2, Normalizer::NFD);

		return strcmp($str1, $str2);
	}

	public static function strncmp(string $str1, string $str2, int $len) : int
	{
		$str1 = mb_substr($str1, 0, $len);
		$str2 = mb_substr($str2, 0, $len);
		return static::strcmp($str1, $str2);
	}

	/**
	 * Conversion wrappers for brewity
	 */
	public static function isUtf8(string $str) : bool
	{
		return mb_detect_encoding($str, self::ENCODING_UTF8, true) === self::ENCODING_UTF8;
	}

	public static function toUtf8(string $str, string $from_encoding = self::ENCODING_INTERNAL) : string
	{
		if ($from_encoding === self::ENCODING_UTF8 || static::isUtf8($str))
			return $str;

		return mb_convert_encoding($str, self::ENCODING_UTF8, $from_encoding);
	}

	public static function fromUtf8(string $str, string $to_encoding = self::ENCODING_INTERNAL) : string
	{
		if ($to_encoding === self::ENCODING_UTF8)
			return $str;

		return mb_convert_encoding($str, $to_encoding, self::ENCODING_UTF8);
	}

	/**
	 * Wrappers for mbstring versions for consistency
	 */
	public static function strrpos(string $str, string $needle, int $offset = 0)
	{
		if (empty($str))
			return false;

		return mb_strrpos($str, $needle, $offset);
	}

	public static function strpos(string $str, string $needle, int $offset = 0)
	{
		if (empty($str))
			return false;

		return mb_strpos($str, $needle, $offset);
	}

	public static function strtolower(string $str) : string
	{
		return mb_strtolower($str);
	}

	public static function strtoupper(string $str) : string
	{
		return mb_strtoupper($str);
	}

	public static function substr(string $str, int $offset, ?int $length = null) : string
	{
		// mb_substr length is null tolerant
		return mb_substr($str, $offset, $length);
	}

	public static function substr_count(string $str, string $needle, int $offset = 0, ?int $len = null) : int
	{
		// mb_substr_count is all but useless in reality
		return substr_count(mb_substr($str, $offset, $len), $needle);
	}

 	public static function strstr(string $str, string $needle, bool $before_needle = false)
 	{
		if (empty($str))
			return false;

 		return mb_strstr($str, $needle, $before_needle);
 	}

  	public static function strrchr(string $str, string $needle, bool $before_needle = false)
 	{
		if (empty($str))
			return false;

 		return mb_strrchr($str, $needle, $before_needle);
 	}

 	public static function strlen(string $str) : int
	{
		return mb_strlen($str);
	}

 	public static function bstrlen(string $str) : int
	{
		// alias this under a more semantic name
		return strlen($str);
	}
}
