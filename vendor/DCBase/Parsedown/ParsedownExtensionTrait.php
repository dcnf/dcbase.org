<?php
/**
*
* @package dcbase-parsedown
* @copyright (c) 2019 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Parsedown;

/**
 * This trait adds minor enhancement to Parsedown's implementation of markdown table syntax
 *
 * Portions of the code in this trait has been lifted from ParsedownExtra so that the Trait can
 * be used regardless of the flavour of Parsedown being used.
 *
 * Note: use non multibyte string functions because PREG_OFFSET_CAPTURE returns offsets that are
 * not multibyte aware (consequently neither is Parsedown).
 */
trait ParsedownExtensionTrait
{
	// Hold default style information
	protected $defaultStyles = array();

	public function __construct()
	{
		if (is_callable('parent::__construct'))
			parent::__construct();

		$this->safeLinksWhitelist[] = 'adc://';
		$this->safeLinksWhitelist[] = 'adcs://';
		$this->safeLinksWhitelist[] = 'dchub://';
	}

	public function setDefaultTableStyle($styles)
	{
		if (!is_array($styles))
			$styles = $this->parseAttributeData($styles);

		$this->defaultStyles['table'] = $styles;
	}

	public function setDefaultBlockquoteStyle($styles)
	{
		if (!is_array($styles))
			$styles = $this->parseAttributeData($styles);

		$this->defaultStyles['blockquote'] = $styles;
	}

	protected function blockTable($Line, array $Block = null)
	{
		// Base parsedown blockTable is stricter now so we need to do the check pre-emptively
 		if (!isset($Block) or $Block['type'] !== 'Paragraph' or isset($Block['interrupted']))
			return;

		$matches = array();
		if (preg_match('/\|[ ]*{(' . $this->regexAttribute . '+)}[ ]*$/', $Block['element']['handler']['argument'], $matches, PREG_OFFSET_CAPTURE))
		{
			$attributes = $this->parseAttributeData($matches[1][0]);
			$Block['element']['handler']['argument'] = substr($Block['element']['handler']['argument'], 0, $matches[0][1]);
		}

		$Block = parent::blockTable($Line, $Block);
		if (isset($Block) and $Block['element']['name'] === 'table')
		{
			if (!empty($attributes))
			{
				$Block['element']['attributes'] = $attributes;
			}
			else if (isset($this->defaultStyles['table']))
			{
				$Block['element']['attributes'] = $this->defaultStyles['table'];
			}
		}
		else if (!empty($matches))
		{
			// This wasn't actually a valid table, restore any captured text
			$Block['element']['handler']['argument'] .= $matches[0][0];
		}

		return $Block;
	}

	protected function blockQuote($Line)
	{
		$Block = parent::blockQuote($Line);
		if (isset($Block) and $Block['element']['name'] === 'blockquote')
			$Block['element']['attributes'] = $this->defaultStyles['blockquote'];

		return $Block;
	}

	//
	// Code below is adapted from ParsedownExtra to be available regardless of which class uses this trait
	//

	protected function parseAttributeData($attributeString)
	{
		$attributes = array();
		if (strpos($attributeString, ' ') === false)
		{
			// this permits a more CSS like input
			$attributeString = ltrim(str_replace(array('#', '.'), array(' #', ' .'), $attributeString), ' ');
		}

		// rest of this method is functionally identical to ParsedownExtra
		$attributes = preg_split('/[ ]+/', $attributeString, - 1, PREG_SPLIT_NO_EMPTY);

		$Data = array();
		foreach ($attributes as $attribute)
		{
			if (!isset($attribute[1]))
				continue;

			if ($attribute[0] === '#')
			{
				$Data['id'] = substr($attribute, 1);
			}
			else if ($attribute[0] === '.')
			{
				$Data['class'][] = substr($attribute, 1);
			}
		}

		if (isset($Data['class']))
			$Data['class'] = implode(' ' , $Data['class']);

		return $Data;
	}

	// note: this pattern has to match ParsedownExtra exactly, because PHP is weird :)
	protected $regexAttribute = '(?:[#.][-\w]+[ ]*)';
}
