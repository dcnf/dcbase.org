<?php
/**
*
* @package dcbase-parsedown
* @copyright (c) 2019 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Parsedown;

use ParsedownExtra as ParsedownExtraBase;

use DCBase\Parsedown\ParsedownExtensionTrait;

/**
 * Class stub for importing ParsedownExtensionTrait
 */
class ParsedownExtra extends ParsedownExtraBase
{
	use ParsedownExtensionTrait;
}
