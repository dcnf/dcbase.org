<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;

use DCBase\Common\Util;

/**
 * Class for loading config files
 */
class ConfigLoader
{
	// private constructor so no instances can be created
	final private function __construct() { }

	/**
	 * Load config file(s)
	 */
	public static function load($area)
	{
		if (is_string($area))
			return static::loadFile($area);

		if (is_array($area))
		{
			$config = array();
			foreach ($area as $key)
				$config[$key] = static::loadFile($key);
			return $config;
		}

		throw new Exception('Invalid argument for ConfigLoader::load().');
	}

	/**
	 * Config value lookup
	 */
	public static function getValue(string $key, string $area)
	{
		static $values = array();
		if (!isset($values[$area]))
			$values[$area] = static::load($area);

		$pieces = explode('.', $key);
		$config = $values[$area];
		foreach ($pieces as $value)
		{
			if (!isset($config[$value]))
				return null;

			$config = $config[$value];
			if (!is_array($config) || !Util::isAssocArray($config))
				return $config;
		}

		throw new Exception("Ambiguous config key '$key'");
	}

	public static function getSecret(string $key)
	{
		return static::getValue($key, 'SecretSauce');
	}

	protected static function loadFile(string $area)
	{
		// this function makes use of the ability to use the return statement inside an included file
		// and validates the file based on it (include returns 1 on files without return statement).
		$config_var = null;

		$config_file = DCBASE_CONFIG_PATH . "$area." . PHP_EXT;
		if (is_file($config_file))
			$config_var = require($config_file);

		// throw on an unexpected value
		if ($config_var === null || (!is_array($config_var) && !is_bool($config_var)))
			throw new Exception("Unable to load config file '$area'.");

		return $config_var;
	}
}
