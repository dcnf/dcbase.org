<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;

use DCBase\WebRequest;
use DCBase\Common\StrUtil as str;
use DCBase\DirectoryList;

/**
 * Web response base class
 */
abstract class WebResponse
{
	// Supported response types
	const TWIG_RESPONSE = 'Twig';
	const FILE_RESPONSE = 'File';
	const TEXT_RESPONSE = 'Text';
	const XML_RESPONSE  = 'Xml';
	const JSON_RESPONSE = 'Json';

	// Common HTTP response codes
	const HTTP_OK = 200;

	// HTTP request
	protected $request;

	// The character encoding of the response
	protected $charset;

	// HTTP status code for the response
	protected $status_code;
	
	// Custom HTTP headers from controller
	private $headers = array();

	// HTTP status codes and messages (from HTTP specification)
	protected static $messages = array(
		// Informational 1xx
		100 => 'Continue',
		101 => 'Switching Protocols',

		// Success 2xx
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',

		// Redirection 3xx
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found', // 1.1
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		// 306 is deprecated but reserved
		307 => 'Temporary Redirect',
		308 => 'Permanent Redirect',

		// Client Error 4xx
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',

		// Server Error 5xx
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
		509 => 'Bandwidth Limit Exceeded'
	);

	/**
	 * Common factory method for HTTP Redirects
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $url the URL to redirect the browser to.
	 * @param integer $status_code the HTTP status code for the redirect, 200 for HTML redirect or 30X for location redirect.
	 * @param string|null $message optional to be message included in the response's HTML body.
	 * @param integer $time time to wait until user is redirected for HTML based redirects.
	 * 
	 * @return WebResponse|null returns WebResponse representing the redirect may return null in case of recoverable error (check with instanceof).
	 */
	public static function redirect(WebRequest $request, string $url, int $status_code = WebResponse::HTTP_OK, ?string $message = null, int $time = 5) : ?WebResponse
	{
		$response = static::create($request, WebResponse::TWIG_RESPONSE, $status_code);
		$url = $request->makeURL($url);

		$response->header('Refresh', "$time; url=$url");
		if ($time == 0 || $status_code == 302 || $status_code == 301 || $status_code == 307 || $status_code == 308)
			$response->header('Location', $url);
	
		// page variables
		$page_vars = array(
			'status_code'		=> $status_code,
			'status_message'	=> static::$messages[$status_code],
			'redirect_url'		=> $url,
			'redirect_time'		=> $time
		);

		if ($message !== null)
			$page_vars['message'] = $message;

		return $response->body('@template/redirect.html.twig', $page_vars);
	}

	/**
	 * Common factory method for HTTP error pages.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param integer $status_code the HTTP status code for the error MUST be >= 400
	 * @param string|null $message optional to be message included in the response's HTML body.
	 * 
	 * @return WebResponse|null returns WebResponse representing the error may return null in case of recoverable error (check with instanceof).
	 * @throws Exception if the provided status code is not an HTTP error code.
	 */
	public static function error(WebRequest $request, int $status_code, ?string $message = null) : ?WebResponse
	{
		if ($status_code < 400)
			throw new Exception('Provided HTTP status code is not an error code');

		// page variables
		$page_vars = array(
			'status_code'		=> $status_code,
			'status_message'	=> static::$messages[$status_code]
		);

		if ($message !== null)
			$page_vars['message'] = $message;

		return static::create($request, WebResponse::TWIG_RESPONSE, $status_code)->body('@template/error.html.twig', $page_vars);
	}

	/**
	 * Factory method for responses of Markdown files.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $file_path path style reference to the file to serve, relative to content root or otherwise implementation resolvable.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the rendered file may return null in case of recoverable error (check with instanceof).
	 */
	public static function markdown(WebRequest $request, string $file_path, int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::TWIG_RESPONSE, $status_code)->body('@template/markdown_view.html.twig', array(
			'markdown_file' => $file_path
		));
	}

	/**
	 * Factory method for responses for embedding PDF files.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $file_path path style reference to the file to serve, relative to content root or otherwise implementation resolvable.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the rendered file may return null in case of recoverable error (check with instanceof).
	 */
	public static function pdf(WebRequest $request, string $file_path, int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::TWIG_RESPONSE, $status_code)->body('@template/pdf_view.html.twig', array(
			'source_file' => $file_path
		));
	}

	/**
	 * Factory method for responses that render text based files as source code.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $file_path path style reference to the file to serve, relative to content root or otherwise implementation resolvable.
	 * @param string $language string reference to the programming language (e.g. 'javascript') or syntax the file is in (e.g. 'css')
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the rendered file may return null in case of recoverable error (check with instanceof).
	 */
	public static function source(WebRequest $request, string $file_path, string $language, int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::TWIG_RESPONSE, $status_code)->body('@template/source_view.html.twig', array(
			'source_file' => $file_path,
			'source_lang' => $language
		));
	}

	/**
	 * Factory method for responses that render (index) directory contents as HTML.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $base_path the path to consider as the "root" for the indexed directory.
	 * @param string $path_fragment the relative path fragment to render, relative to the base path above.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the rendered directory may return null in case of recoverable error (check with instanceof).
	 */
	public static function directory(WebRequest $request, string $base_path, string $path_fragment = '', int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		// This is slightly inefficient because DirectoryList was originally written to work with unchecked paths (including file paths)
		$index = new DirectoryList($base_path, $path_fragment);

		if (($files = $index->listDirectory($request)) !== false)
			return static::create($request, WebResponse::TWIG_RESPONSE, $status_code)->body('@template/directory_index.html.twig', $files);

		return static::error($request, 403);
	}

	/**
	 * Factory method for HTML pages using Twig templates.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $template reference to the Twig template to render.
	 * @param array $page_vars variables to set into the template context, globally usable inside the template.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the page may return null in case of recoverable error (check with instanceof).
	 */
	public static function page(WebRequest $request, string $template, array $page_vars = array(), int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::TWIG_RESPONSE, $status_code)->body($template, $page_vars);
	}

	/**
	 * Factory method for JSON responses, supports JSONP responses through callback request argument.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param mixed $json_data SplFileInfo referencing an accessible (readable) text file, json string or any other non-resource type that can be encoded as json.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the JSON response may return null in case of recoverable error (check with instanceof).
	 */
	public static function json(WebRequest $request, $json_data, int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::JSON_RESPONSE, $status_code)->body($json_data);
	}

	/**
	 * Factory method for XML reponses
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param mixed $xml_data DOMDocument|SimpleXmlElement|SplFileInfo or a string that is either XML or a reference to an accessible (readable) path to an XML file.
	 * @param string|null $mime_type optional mime type to serve the file as, defaults to application/xml.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the XML response may return null in case of recoverable error (check with instanceof).
	 */
	public static function xml(WebRequest $request, $xml_data, ?string $mime_type = null, int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::XML_RESPONSE, $status_code)->body($xml_data, $mime_type);
	}

	/**
	 * Factory method for plain text responses.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param mixed $text_data SplFileInfo or string representing an accessible (readable) path to a text file.
	 * @param string|null $mime_type optional mime type to serve the file as, defaults to text/plain.
	 * @param integer $status_code the HTTP status code for the response.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the text response may return null in case of recoverable error (check with instanceof).
	 */
	public static function text(WebRequest $request, $text_data, ?string $mime_type = null, int $status_code = WebResponse::HTTP_OK) : ?WebResponse
	{
		return static::create($request, WebResponse::TEXT_RESPONSE, $status_code)->body($text_data, $mime_type);
	}

	/**
	 * Factory method for CSS responses, should behave identical to text factory with 'text/css' mime-type.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param mixed $text_data SplFileInfo or string representing an accessible (readable) path to a CSS file.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the css response may return null in case of recoverable error (check with instanceof).
	 */
	public static function css(WebRequest $request, $text_data) : ?WebResponse
	{
		return static::create($request, WebResponse::TEXT_RESPONSE, WebResponse::HTTP_OK)->body($text_data, 'text/css');
	}

	/**
	 * Factory method for JavaScript responses, should behave identical to text factory with 'application/javascript' mime-type.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param mixed $text_data SplFileInfo or string representing an accessible (readable) path to a JavaScript file.
	 * 
 	 * @return WebResponse|null returns WebResponse representing the JavaScript response may return null in case of recoverable error (check with instanceof).
	 */
	public static function javascript(WebRequest $request, $text_data) : ?WebResponse
	{
		return static::create($request, WebResponse::TEXT_RESPONSE, WebResponse::HTTP_OK)->body($text_data, 'application/javascript');
	}

	/**
	 * Factory method for serving other arbitrary file types, mime type is detected based on extension.
	 * 
	 * This should always be the preferred response type for larger files regardless of their type.
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param mixed $file SplFileInfo or string representing an accessible (readable) path to a file.
	 * @param boolean $force_download tells the browser to always save instead of possibly displaying the file.
	 * @param string|null $serve_from path to serve the file from to use with 'X-Accel-Redirect' header @see NGINX_USE_X_ACCEL_REDIRECT
	 * 
 	 * @return WebResponse|null returns WebResponse representing the file response may return null in case of recoverable error (check with instanceof).
	 */
	public static function file(WebRequest $request, $file, bool $force_download = false, ?string $serve_from = null) : ?WebResponse
	{
		return static::create($request, WebResponse::FILE_RESPONSE, WebResponse::HTTP_OK)->body($file, $force_download, $serve_from);
	}

	/**
	 * The main general purpose response factory that others delegate to. 
	 * 
	 * @param WebRequest $request the incoming WebRequest instance.
	 * @param string $type one of the response type constants defined by this interface.
	 * @param integer $status_code the HTTP status code for the response.
	 * @param string|null $charset the character set this response is in (defaults to php 'default_charset' setting or UTF-8 if null)
	 * 
	 * @return WebResponse|null returns a new WebResponse object may return null in case of recoverable error (check with instanceof).
	 * @throws Exception if implementation for the specified type can not be found or it is of incorrect type.
	 */
	public static function create(WebRequest $request, string $type, int $status_code = WebResponse::HTTP_OK, ?string $charset = null) : ?WebResponse
	{
		if (empty($charset))
		{
			$default_charset = ini_get('default_charset');
			$charset = str::strtolower($default_charset !== false ? $default_charset : str::ENCODING_UTF8);
		}

		if (!isset(static::$messages[$status_code]))
			throw new Exception('Invalid HTTP status code provided');

		$class = "\\DCBase\\Response\\{$type}Response";
		$response = new $class($request, $status_code, $charset);
		if ($response instanceof WebResponse)
			return $response;

		throw new Exception('Created respnse is not of type WebResponse');
	}

	protected function __construct(WebRequest $request, int $status_code, string $charset)
	{
		$this->request = $request;
		$this->charset = $charset;
		$this->status_code = $status_code;

		$this->headers = array(
			'Content-Type' 	=> array("text/html; charset=$charset"),
			'Cache-Control'	=> array('private, no-cache="set-cookie"'),
			'Expires'		=> array('0'),
			'Pragma'		=> array('no-cache')
		);

		if (defined('DCBASE_EXPOSE') && DCBASE_EXPOSE)
			$this->headers['X-Powered-By'] = array('DCBase');
	}

	/**
	 * Set an HTTP response header to be sent with the response.
	 * 
	 * @param string $name the name of the response header to set.
	 * @param mixed $value the header value to set, can be any type with string representation that does not contain EOL characters.
	 * @param boolean $replace whether to replace a previously set header or set a second value (send two headers).
	 * 
	 * @return boolean true if the header set is modifiable (i.e. headers are not sent).
	 */
	public function header(string $name, $value, bool $replace = true) :  bool
	{
		if (headers_sent())
			return false;

		if ($replace && isset($this->headers[$name]))
		{
			$this->headers[$name] = array($value);
		}
		else
		{
			$this->headers[$name][] = $value;
		}

		return true;
	}

	private function sendStatusCode(int $code) : void
	{
		$message = static::$messages[$code];
		if (substr(strtolower(php_sapi_name()), 0, 3) === 'cgi')
		{
			header("Status: $code $message", true, $code);
		}
		else
		{
			$version = $this->request->server('SERVER_PROTOCOL', 'HTTP/1.0');
			header("$version $code $message", true, $code);
		}
	}

	/**
	 * Remove an HTTP response header from the set of headers to send by name.
	 * 
	 * @param string $name the header to remove.
	 * 
	 * @return boolean true if the header set is modifiable (i.e. headers are not sent).
	 */
	public function removeHeader(string $name) : bool
	{
		return static::removeHeaders(array($name));
	}

	/**
	 * Remove multiple HTTP response headers from the set of headers to send.
	 * 
	 * @param array $names the set of header names to remove.
	 * 
	 * @return boolean true if the header set is modifiable (i.e. headers are not sent).
	 */
	public function removeHeaders(array $names) : bool
	{
		if (headers_sent())
			return false;

		foreach ($names as $name)
		{
			if (isset($this->headers[$name]))
				unset($this->headers[$name]);
		}

		return true;
	}

	/**
	 * Remove all previously set headers
	 * 
	 * @return boolean true if the header set is modifiable (i.e. headers are not sent).
	 */
	public function clearHeaders() : bool
	{
		if (headers_sent())
			return false;

		$this->headers = array();
		return true;
	}

	protected function sendHeaders() : bool
	{
		if (headers_sent())
			return false;

		$this->sendStatusCode($this->status_code);
		foreach ($this->headers as $name => $values)
		{
			foreach ($values as $key => $value)
				header("$name: $value", $key == 0);
		}

		return true;
	}

	/**
	 * Send the response to browser, optionally stop execution afterwards.
	 * 
	 * @param boolean $exit call exit to end execution.
	 */
	public function send(bool $exit = false) : void
	{
		$this->sendHeaders();
		echo $this->output();

		if ($exit) exit(0);
	}

	/**
	 * Set the body for this response.
	 * 
	 * @param mixed $content reference to the content of the body, may vary between response types which may also take additional arguments.
	 * 
	 * @return WebResponse returns the current (this) response instance.
	 */
	abstract public function body($content) : WebResponse;

	/**
	 * Return the fully processed body of this response.
	 * 
	 * @return string|null the response content or null if there is no string representation, or such representation is not suitable for storing in memory.
	 */
	abstract public function output() : ?string;
}
