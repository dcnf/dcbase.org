<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Handler;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use SplFileInfo;

use DCBase\Handler\AbstractHandler;
use DCBase\WebRequest;
use DCBase\Response\ResponseHelper;
use DCBase\WebResponse;

/**
 * Handler which serves fixed set of files outside of DCBASE_CONTENT_ROOT and provides fallbacks for subdomains
 */
class ExtFileHandler extends AbstractHandler
{
	/**
	 * Handle an incoming request for a resource outside of FrontController
	 * 
	 * @param  WebRequest       $request       The incoming request expected to be handled
	 * @param  string           $relative_path The relative file path as part of the query string
	 * @param  SplFileInfo|null $file          File object bound to a document root, referenced path may not exist and cannot be a symlink
	 * @return mixed                           Valid WebResponse instance to be served, any other values (e.g. false) will continue processing of the request in FrontController
	 */
	public function handleResource(WebRequest $request, string $relative_path, ?SplFileInfo $file = null) : ?WebResponse
	{
		// if the file resolved by FrontController exists, prefer it instead
		if ($file->isFile())
			return null;

		// fallback for subdomains, serve robots.txt and favicon.ico
		$filename = $request->getPathSegment('filename');
		if ($filename === 'robots.txt' || $filename === 'favicon.ico')
		{
			$candidate = new SplFileInfo(DCBASE_CONTENT_ROOT . $filename);
			if ($candidate->isFile())
				return WebResponse::file($request, $candidate);
		}

		// serve hardcoded files from git root (request uri may be prefixed with 'DCBase/', see HandlerMap config file)
		if ($filename === 'LICENSE' || $filename === 'DCBase/LICENSE')
		{
			return WebResponse::text($request, new SplFileInfo(DCBASE_ROOT_PATH . 'LICENSE'));
		}

		if ($filename === 'README' || $filename === 'DCBase/README')
		{
			$response = ResponseHelper::serveMarkdownFile($request, new SplFileInfo(DCBASE_ROOT_PATH . 'README.md'));
			if ($response instanceof WebResponse)
				return $response;
		}

		return false;
	}
}
