<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Handler;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use SplFileInfo;

use DCBase\WebRequest;
use DCBase\WebResponse;

/**
 * Handler base class for extending FrontController functionality
 */
abstract class AbstractHandler
{
	/**
	 * Constructor
	 */
	public function __construct() { }

	/**
	 * Handle an incoming request for a resource outside of FrontController
	 * 
	 * @param  WebRequest       $request       The incoming request expected to be handled
	 * @param  string           $relative_path The relative file path as part of the query string
	 * @param  SplFileInfo|null $file          File object bound to a document root, referenced path may not exist and cannot be a symlink
	 * @return mixed                           Valid WebResponse instance to be served, any other values (e.g. false) will continue processing of the request in FrontController
	 */
	abstract public function handleResource(WebRequest $request, string $relative_path, ?SplFileInfo $file = null) : ?WebResponse;
}
