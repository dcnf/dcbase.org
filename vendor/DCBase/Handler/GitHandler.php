<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Handler;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use SplFileInfo;

use DCBase\Common\GitUtil;
use DCBase\Handler\AbstractHandler;
use DCBase\Response\TwigResponse;
use DCBase\WebRequest;
use DCBase\WebResponse;
use DCBase\ConfigLoader;

use DCBase\Twig\TextFormat\TextFormatExtension;

/**
 * Handler invoked by git web hooks form e.g. gitlab or github
 */
class GitHandler extends AbstractHandler
{
	/**
	 * Handle an incoming request for a resource outside of FrontController
	 * 
	 * @param  WebRequest       $request       The incoming request expected to be handled
	 * @param  string           $relative_path The relative file path as part of the query string
	 * @param  SplFileInfo|null $file          File object bound to a document root, referenced path may not exist and cannot be a symlink
	 * @return mixed                           Valid WebResponse instance to be served, any other values (e.g. false) will continue processing of the request in FrontController
	 */
	public function handleResource(WebRequest $request, string $relative_path, ?SplFileInfo $file = null) : ?WebResponse
	{
		if (!GitUtil::isGit(DCBASE_ROOT_PATH))
			return WebResponse::error($request, 404);

		$hook = $request->getPathSegment('hook');
		$api_token = $request->getPathSegment('api_token');
		$api_secret = ConfigLoader::getSecret('git.webhooks_token');

		if ($api_token !== $api_secret)
			return WebResponse::error($request, 403);

		$hook_data = json_decode((string) @file_get_contents('php://input'));
		if (!is_object($hook_data) || empty($hook_data->ref))
			return WebResponse::error($request, 400);

		if ($hook === 'push')
		{
			$response = $this->onGitPush($request, $hook_data);
			if ($response instanceof WebResponse)
				return $response;
		}

		return WebResponse::error($request, 400);
	}

	private function onGitPush(WebRequest $request, $input)
	{
		if (!($request->hasHeader('X-Gitlab-Event') || $request->hasHeader('X-GitHub-Event')) || $input->ref !== GitUtil::REF_MASTER)
			return WebResponse::error($request, 403);

		clearstatcache();

		GitUtil::pruneGitCache();

		// save the payload to a location monitored by the server OS for changes, should trigger a pull request to update site files
		if (!GitUtil::putGitCache('git_master.json', $input))
			return WebResponse::error($request, 500);

		TwigResponse::getTwig($request)->getExtension(TextFormatExtension::class)->pruneContentCache();

		return WebResponse::text($request, "OK");
	}
}
