<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Throwable;

use DCBase\Common\Util;

/**
 * Static class for error reporting functions
 */
class ErrorReporting
{
	// private constructor so no instances can be created
	final private function __construct() { }

	/**
	 * Format PHP errors and warnings
	 */
	public static function formatError(int $errno, string $message, ?string $file, ?int $line, bool $html) : string
	{
		$message = static::filterPaths($message);
		if (!empty($file))
			$file = static::filterPaths($file);

		$error_name = 'Error';
		switch ($errno)
		{
			case E_NOTICE:
			case E_USER_NOTICE:
				$error_name = 'Notice';
				break;
			case E_WARNING:
			case E_USER_WARNING:
			case E_DEPRECATED:
			case E_STRICT:
				$error_name = 'Warning';
				break;
		}

		$error_format = '[Debug] %s: in file %s, on line %d: %s' . PHP_EOL;
		if ($html)
		{
			$error_format = '<strong>[Debug] %s:</strong> in file <strong>%s</strong>, on line <strong>%d</strong>: <strong>%s</strong><br />';
			$file = htmlspecialchars($file);
			$message = htmlspecialchars($message);
		}

		return sprintf($error_format . PHP_EOL, $error_name, (string) $file, (int) $line, $message);
	}

	/**
	 * Format any exception that extends the standard PHP Exception class
	 */
	public static function formatException(Throwable $exception, bool $html) : string
	{
		$message = static::filterPaths($exception->getMessage());
		$file = $exception->getFile();
		if (!empty($file))
			$file = static::filterPaths($file);

		$error_format = '';
		if (!$html)
		{
			$error_format = 'MESSAGE: %s' . PHP_EOL . 'FILE: %s' . PHP_EOL . 'LINE: %d' . PHP_EOL;
			$error_format = sprintf($error_format, $message, $file, $exception->getLine());

			if (Util::isDebug())
				$error_format .= PHP_EOL  . 'BACKTRACE:' . PHP_EOL . static::formatTrace($exception->getTrace(), $html);
		}
		else
		{
			$error_format = '<strong>MESSAGE:</strong> <em>%s</em> <br /> <strong>FILE:</strong> <em>%s</em> <br /> <strong>LINE:</strong> <em>%d</em>' . PHP_EOL;
			$error_format = sprintf($error_format, $message, $file, $exception->getLine());

			if (Util::isDebug())
				$error_format .= '<br /><br /> BACKTRACE: <br />' . static::formatTrace($exception->getTrace(), $html) . PHP_EOL;
		}

		return $error_format;
	}

	/**
	 * Format exception stack traces
	 */
	public static function formatTrace(array $trace, bool $html) : string
	{
		$output = '';
		foreach ($trace as $frame)
		{
			// Strip the current directory from path
			$frame['file'] = empty($frame['file']) ? '(not given by php)' : static::filterPaths($frame['file']);
			$frame['line'] = empty($frame['line']) ? '(not given by php)' : $frame['line'];

			// Skip error and exception handlers
			if (in_array($frame['function'], array('errorHandler', 'exceptionHandler')))
				continue;

			// Only show function arguments for include etc. otherwise use the type
			foreach ($frame['args'] as $key => $argument)
			{
				if ($key == 0 && in_array($frame['function'], array('include', 'require', 'include_once', 'require_once')))
				{
					$frame['args'][$key] = '\''. static::filterPaths($argument) .'\'';
					continue;
				}
				$frame['args'][$key] = is_object($argument) ? get_class($argument) : gettype($argument);
			}

			$frame['class'] = (!isset($frame['class'])) ? '' : $frame['class'];
			$frame['type'] = (!isset($frame['type'])) ? '' : $frame['type'];

			$output .= PHP_EOL;
			$output .= 'FILE: ' . $frame['file'] . PHP_EOL;
			$output .= 'LINE: ' . $frame['line'] . PHP_EOL;

			$output .= 'CALL: ' . $frame['class'] . $frame['type'] . $frame['function'];
			$output .= '(' . implode(', ', $frame['args']) . ')' . PHP_EOL;
		}

		if (!$html)
			return $output;

		$output = nl2br(htmlspecialchars($output));
		return '<div style="font-family: monospace">'. $output .'</div>';
	}

	/**
	 * Helper: filter server paths from output
	 */
	protected static function filterPaths(string $message) : string
	{
		$message = str_replace(DCBASE_CONTENT_ROOT, '[CONTENT]/', $message);
		$message = str_replace(DCBASE_CACHE_PATH, '[CACHE]/', $message);
		$message = str_replace(DCBASE_ROOT_PATH, '[ROOT]/', $message);
		return $message;
	}
}
