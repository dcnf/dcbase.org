<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use ArrayAccess;

use DCBase\Common\StrUtil as str;
use DCBase\Handler\AbstractHandler;
use DCBase\Request\RequestHelper;

/**
 * Class handling arguments for HTTP requests
 *
 * Portions of the code in this class, particularly significant parts of its public interface definitions, are inspired by or
 * derived from code originally part of the phpbb_request class by Nils Aderman.
 * 
 * @see https://github.com/phpbb/phpbb/blob/feature/ascraeus-experiment/phpBB/includes/core/request.php (retrieved: 05-21-2012)
 * @author naderman
 * @copyright (c) 2010 phpBB Group
 */
class WebRequest implements ArrayAccess
{
	const POST = 0;
	const GET = 1;
	const REQUEST = 2;
	const SERVER = 3;
	const COOKIE = 4;
	const FILES = 5;

	private $input = array();

	// URI cache
	private $base_url = null;
	private $request_url = null;
	private $content_root = null;

	// Processed path info for this request, if any...
	private $route = null;

	public static function create(array $input = array())
	{
		return new static($input);
	}

	protected function __construct(array $input)
	{
		$this->input = $input;

		if (empty($this->input))
		{
			// store globals
			$this->input[self::POST] =& $_POST;
			$this->input[self::GET] =& $_GET;
			$this->input[self::SERVER] =& $_SERVER;
			$this->input[self::COOKIE] =& $_COOKIE;
			$this->input[self::FILES] =& $_FILES;

			// build REQUEST array (GET has preference in case of equal keys)
			$this->input[self::REQUEST] = $this->input[self::GET] + $this->input[self::POST];
		}
	}

	public function exists(string $name, int $global = self::REQUEST) : bool
	{
		return isset($this->input[$global][$name]);
	}

	public function isEmpty(int $global = self::REQUEST) : bool
	{
		return empty($this->input[$global]);
	}

	public function variable(string $name, $default, int $global = self::REQUEST, bool $multibyte = false)
	{
		if (!isset($this->input[$global][$name]))
			return $default;

		$value = $this->input[$global][$name];
		RequestHelper::typecast($value, gettype($default), $multibyte);

		return $value;
	}

	public function server(string $name, $default = '')
	{
		// Support reverse proxies, technically nginx with HttpRealipModule doesn't need this.
		if (defined('IP_HEADER') && $name == 'REMOTE_ADDR')
			return $this->header(IP_HEADER, $default);

		return $this->variable($name, $default, self::SERVER, true);
	}

	public function hasHeader(string $header_name) : bool
	{
		$name = 'HTTP_' . str_replace('-', '_', strtoupper($header_name));
		return $this->exists($name, self::SERVER);
	}

	public function header(string $header_name, $default = '')
	{
		$name = 'HTTP_' . str_replace('-', '_', strtoupper($header_name));
		return $this->server($name, $default);
	}

	public function host() : string 
	{
		return $this->header('Host', $this->server('SERVER_NAME', ''));
	}

	public function secure() : bool
	{
		return $this->server('HTTPS', '') !== '';
	}

	public function ajax() : bool
	{
		// Note: Google Visualization API does not set X-Requested-With
		return $this->header('X-Requested-With', '') === 'XMLHttpRequest';
	}

	/**
	 * Function for resolving virtual document root
	 */
	public function getContentRoot() : string
	{
		if ($this->content_root === null)
		{
			$this->content_root = DCBASE_CONTENT_ROOT;
			$current_host = $this->host();

			$hosts = ConfigLoader::load('HostMap');
			if (isset($hosts[$current_host]))
				$this->content_root = $hosts[$current_host];

			// TODO: make this smarter (ie. toplevel domains like .co.uk)
			$domain = explode('.', $current_host, 3);
			if (sizeof($domain) > 2)
			{
				$subdomain = array_shift($domain);
				$domain = implode('.', $domain);

				if (isset($hosts[$domain]))
					$this->content_root = $hosts[$domain];

				if (is_dir($this->content_root . $subdomain . '/'))
					$this->content_root .= $subdomain . '/';
			}
		}

		return $this->content_root;
	}

	/**
	 * Functions for URL generation
	 */
	public function getRequestURL() : string
	{
		if ($this->request_url === null)
		{
			$this->request_url = $this->getBaseURL();

			// path info
			$path_info = rtrim($this->getPathInfo(), '/');
			if (!empty($path_info))
			{
				// ensure there is a slash separating url and path info regardless of server config
				if ($path_info[0] !== '/')
					$path_info = "/{$path_info}";

				$this->request_url .= $path_info;
			}

			// query string
			$query_string = $this->server('QUERY_STRING', '');
			if (!empty($query_string))
			{
				if ($query_string[0] == '&')
					$query_string = str::substr($query_string, 1);			
				$this->request_url .= "?$query_string";
			}
		}

		return $this->request_url;
	}

	public function getBaseURL() : string
	{
		if ($this->base_url === null)
		{
			// protocol & host
			$secure = $this->secure();
			$this->base_url = ($secure ? 'https://' : 'http://') . $this->host();

			// port
			$port = $this->server('SERVER_PORT', 0);
			if ($port && ((!$secure && $port != 80) || ($secure && $port != 443)))
				$this->base_url .= (str::strpos($this->base_url, ':') === false) ? ':' . $port : '';

			// script path
			$script_name = str_replace(array('\\', '//'), '/', $this->server('SCRIPT_NAME', ''));
			$this->base_url .= str_replace('index.php', '', $script_name);

			// remove ending slash
			$this->base_url = rtrim($this->base_url, '/');
		}

		return $this->base_url;
	}

	public function getPathInfo() : string
	{
		return $this->exists('ORIG_PATH_INFO', self::SERVER) ? $this->server('ORIG_PATH_INFO', '') : $this->server('PATH_INFO', '');
	}

	/**
	 * Parse a path info string against a set of rules, populates named segments and returns an external handler if any
	 * 
	 * @return mixed    Returns true or a handler instance if provided path info was correctly parsed, false otherwise.
	 */
	public function parsePathInfo(array $rules, ?string $path_info = null)
	{
		if (!isset($rules['/']))
			$rules['/'] = array('defaults' => array(), 'handler' => null);

		$path_info = rtrim(($path_info === null ? $this->getPathInfo() : $path_info), '/');
		if (empty($path_info))
			$path_info = '/';

		// prepend back the leading slash (this is done so that document root isn't represented by an empty string)
		if ($path_info[0] !== '/')
			$path_info = "/{$path_info}";

		$handler = null;
		if (isset($rules[$path_info]))
		{
			$this->route =& $rules[$path_info]['defaults'];

			if (isset($rules[$path_info]['handler']))
				$handler = $rules[$path_info]['handler'];
		}
		else
		{
			foreach ($rules as $pattern => $route)
			{
				if (!isset($route['regex']))
					$route['regex'] = null;

				if (!isset($route['handler']))
					$route['handler'] = null;

				if (!RequestHelper::buildPathExpression($pattern, $route['regex']))
					continue;

				$matches = array();
				if (preg_match($pattern, $path_info, $matches))
				{
					$this->route =& $route['defaults'];
					foreach ($this->route as $key => $value)
					{
						if (isset($matches[$key]))
						{
							RequestHelper::typecast($matches[$key], gettype($value), true);
							$this->route[$key] = $matches[$key];
						}
					}

					// we are done
					$handler = $route['handler'];
					break;
				}
			}
		}

		// this should not be done but allow it if the handler instance is created in the config file (bad!)
		if ($handler instanceof AbstractHandler)
			return $handler;

		// if a handler class is provided check that it is valid and instanciate it
		if ($handler !== null && is_subclass_of($handler, AbstractHandler::class))
			return new $handler();

		return is_array($this->route);
	}

	public function getPathSegment($name, $default = null)
	{
		if (!empty($name) && is_array($this->route))
			return isset($this->route[$name]) ? $this->route[$name] : $default;

		return $default;
	}

	/**
	 * Function for URL preprocessing
	 */
	public function makeURL(string $resource, ?string $params = null) : string
	{
		// support relative paths and urls without protocol
		$url = ((!isset($resource[0]) || $resource[0] === '/') && (!isset($resource[1]) || $resource[1] !== '/')) ? $this->getBaseURL() . $resource : $resource;
		$url = (str::strpos($url, '://') === false && str::strpos($url, '//') !== 0) ? ($this->secure() ? 'https://' : 'http://') . $url : $url;
		$url = rtrim($url, '/');

		// detect the delimiter to use for appending $params
		$url_delim = (str::strpos($url, '?') === false) ? '?' : '&';

		// check empty params
		if (empty($params))
			$params = null;

		// get anchor
		$anchor = '';
		if (str::strpos($url, '#') !== false)
		{
			list($url, $anchor) = explode('#', $url, 2);
			$anchor = '#' . $anchor;
		}
		else if ($params && str::strpos($params, '#') !== false)
		{
			list($params, $anchor) = explode('#', $params, 2);
			$anchor = '#' . $anchor;
		}

		if (empty($anchor))
			return $url . ($params !== null ? $url_delim . $params : '');

		if ($params === null)
			return $url . $anchor;

		return $url . $url_delim . $params . $anchor;
	}

	/**
	 * ArrayAccess implementation
	 */
	public function offsetExists($offset) : bool
	{
		return isset($this->input[self::REQUEST][$offset]);
	}
	 
	public function offsetGet($offset)
	{
		// force a typecast to string, and treat as multibyte
		return $this->variable($offset, '', self::REQUEST, true);
	}

	public function offsetSet($offset, $value) : void { trigger_error('Request: Attempt to modify a const object.', E_USER_WARNING); }
	public function offsetUnset($offset) : void { trigger_error('Request: Attempt to modify a const object', E_USER_WARNING); }
}
