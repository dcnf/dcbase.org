<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use FilesystemIterator;
use SplFileInfo;

use DCBase\WebRequest;
use DCBase\Common\StrUtil as str;

class DirectoryList
{
	const COLUMN_NONE = '';
	const COLUMN_NAME = 'name';
	const COLUMN_TIME = 'time';
	const COLUMN_SIZE = 'size';
	const COLUMN_PATH = 'path';

	protected $fragment = '';
	protected $base_path = '';
	protected $real_path = '';

	public function __construct(string $base_path, string $path_fragment = '')
	{
		if (!$this->setLocation($base_path, $path_fragment))
			throw new Exception('Directory list request for non-existent path.');
	}

	public function setLocation(string $base_path, string $path_fragment = '') : bool
	{
		// PHP file functions don't properly handle utf-8 file paths
		$this->base_path = realpath($base_path);
		$this->fragment = $path_fragment;

		$this->real_path = realpath("{$this->base_path}/{$this->fragment}");
		if (str::strncmp($this->real_path, $this->base_path, str::strlen($this->base_path)) != 0)
			return false;

		return file_exists($this->real_path);
	}

	protected function makeBreadcrumbs(string $path_fragment) : array
	{
		$breadcrumbs = array();
		$breadcrumbs[] = array('directory_path' => '/', 'directory_name' => '~');

		if (empty($path_fragment))
			return $breadcrumbs;

		$directories = explode('/',  $path_fragment);
		$current_fragment = '/';

		foreach ($directories as $dir)
		{
			if (empty($dir) || $dir === '.')
				continue;

			$current_fragment .= $dir;
			$breadcrumbs[] = array('directory_path' => $current_fragment, 'directory_name' => $dir);
			$current_fragment .= '/';
		}

		return $breadcrumbs;
	}

	public function listDirectory(WebRequest $request) : ?array
	{
		if (!is_dir($this->real_path))
			return null;

		$sort_column = $request->variable('col', self::COLUMN_NAME);
		$sort_asc = $request->variable('asc', true);

		$files = array('directories' => array(), 'files' => array());
		$files['breadcrumbs'] = $this->makeBreadcrumbs($this->fragment);
		$files['sort_info'] = $this->makeSortInfo($sort_column, $sort_asc);

		$readme_file = $this->findReadme();
		if ($readme_file !== null)
		{
			$extension = pathinfo($readme_file, PATHINFO_EXTENSION);
			$files['readme'] = array('path' => $readme_file, 'type' => 'text');

			if (!empty($extension))
			{
				if ($extension === 'md' || $extension === 'markdown')
					$files['readme']['type'] = 'markdown';

				if ($extension === 'twig')
				{
					$filename = pathinfo($readme_file, PATHINFO_FILENAME);
					$files['readme']['type'] = 'twig';
					if (str::substr($filename, -3) === '.md' || str::substr($filename, -9) === '.markdown')
						$files['readme']['type'] = 'markdown';
				}
			}
		}

		if (!empty($this->fragment))
		{
			$parent_fragment = '/' . str::substr($this->fragment, 0, (int) str::strrpos($this->fragment, '/'));
			$files['parent_directory'] = array(
				'file_name' => '..',
				'file_time' => filemtime($this->base_path . $parent_fragment),
				'file_size' => -1,
				'file_path' => $parent_fragment
			);

			$files['relative_root'] = "/{$this->fragment}/";
		}
		else
		{
			$files['relative_root'] = '/';
		}

		foreach (new FilesystemIterator($this->real_path, FilesystemIterator::SKIP_DOTS | FilesystemIterator::NEW_CURRENT_AND_KEY) as $filename => $fi)
		{
			if (!str::isUtf8($filename) || $filename[0] === '.' || !$fi->isReadable() || str::strpos($filename, '?') !== false)
				continue;

			if ($fi->isDir())
			{
				$files['directories'][$filename] = array(
					'file_name' => $filename,
					'file_time' => $fi->getMTime(),
					'file_size' => -1,
					'file_path' => $files['relative_root'] . $filename
				);
			}
			else
			{
				$extension = str::strtolower($fi->getExtension());

				// if twig file has a second extension, remove the original .twig extension
				if ($extension === 'twig' && str::substr_count($filename, '.') > 1)
				{
					$filename = str::substr($filename, 0, -5);
					$extension = str::substr($filename, str::strrpos($filename, '.') + 1);
				}

				$file_params = null;
				if ($extension === 'twig' || $extension === 'css' || $extension === 'js' || $extension === 'json')
					$file_params = 'source=view';

				$files['files'][$filename] = array(
					'file_name'		=> $filename,
					'file_time'		=> $fi->getMTime(),
					'file_size'		=> $fi->getSize(),
					'file_path'		=> $files['relative_root'] . $filename,
					'file_params'	=> $file_params
				);
			}
		}

		$files['empty_directory'] = (empty($files['directories']) && empty($files['files']));

		if (!empty($sort_column) && !$files['empty_directory'])
		{
			$sort_func = function ($val1, $val2) use($sort_column, $sort_asc) : int
			{
				return static::sortCallback($val1, $val2, $sort_column, $sort_asc);
			};

			if (!empty($files['directories']))
			{
				$dir_list = $files['directories'];
				if (usort($dir_list, $sort_func))
					$files['directories'] = $dir_list;
			}

			if (!empty($files['files']))
			{
				$file_list = $files['files'];
				if (usort($file_list, $sort_func))
					$files['files'] = $file_list;
			}
		}

		return $files;
	}

	public function selectedFile() : ?SplFileInfo
	{
		if (is_dir($this->real_path))
			return null;

		return new SplFileInfo($this->real_path);
	}

	public function relativePath() : string
	{
		return $this->fragment;
	}

	public function loadJSON(string $filename)
	{
		$content = $this->findFile("$filename.json", true);
		if ($content !== null)
			return json_decode($content, true);

		return null;
	}

	public function findReadme(bool $contents = false) : ?string
	{
		static $files = array(
			'README', 'README.twig',
			'README.txt', 'README.txt.twig',
			'README.md', 'README.md.twig',
			'README.markdown', 'README.markdown.twig'
		);

		foreach ($files as $filename)
		{
			$result = $this->findFile($filename, $contents);
			if ($result !== null)
				return $result;
		}

		return null;
	}

	protected function findFile(string $filename, bool $contents = true, ?string $path_fragment = null) : ?string
	{
		if ($path_fragment === null)
			$path_fragment = str::substr($this->real_path, str::strlen($this->base_path));

		$file = new SplFileInfo($this->base_path . "$path_fragment/$filename");
		if ($file->isFile() && $file->isReadable())
		{
			if (!$contents)
				return "$path_fragment/$filename";

			return file_get_contents($file->getRealPath());
		}
		else
		{
			$path_fragment = explode( '/', $path_fragment);
			if (empty($path_fragment[1]))
				return null;

			unset($path_fragment[0], $path_fragment[sizeof($path_fragment)]);
			return $this->findFile($filename, $contents, '/' . implode('/', $path_fragment));
		}
	}

	protected function makeSortInfo(string $sort_column, bool $sort_asc) : array
	{
		static $sort_info = array(
			self::COLUMN_NAME => array(),
			self::COLUMN_TIME => array(),
			self::COLUMN_SIZE => array(),
			self::COLUMN_PATH => array()
		);

		foreach ($sort_info as $column => $args)
		{
			$is_sorted = ($column === $sort_column);
			$sort_info[$column] = array(
				'sort_direction'	=> $is_sorted ? (int) $sort_asc : -1,
				'sort_params'		=> "col=$column" . '&asc=' . ($is_sorted ? (int) !$sort_asc : 1)
			);

			if ($is_sorted)
			{
				$sort_info['current'] = array(
					'sort_direction'	=> $sort_info[$column]['sort_direction'],
					'sort_params'		=> "col=$column&asc=" . (int) $sort_asc,
					'sort_column'		=> $column
				);
			}
		}

		return $sort_info;
	}

	protected function sortCallback($val1, $val2, string $sort_column, bool $sort_asc) : int
	{
		if (!isset($val1["file_{$sort_column}"]) || !isset($val2["file_{$sort_column}"]))
			return 0;

		$val1 = $val1["file_{$sort_column}"];
		$val2 = $val2["file_{$sort_column}"];

		if (!is_string($val1) && !is_string($val2))
			return  $sort_asc ? $val1 - $val2 : $val2 - $val1;

		$result = str::strnatcmp($val1, $val2);
		return $sort_asc ? $result : - $result;
	}
}
