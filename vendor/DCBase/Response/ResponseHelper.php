<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Response;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use SplFileInfo;

use DCBase\WebRequest;
use DCBase\WebResponse;

use DCBase\Common\StrUtil as str;

/**
 * Static class for helper functions to serve responses
 *
 * The methods in this class are esesntially an extended set of factories for WebResponses
 * that convert a file path or SplFileInfo to an appropriate WebResponse object.
 */
class ResponseHelper
{
	/// private final constructor so no instances can be created
	final private function __construct() { }

	public static function getTemplateName(WebRequest $request, SplFileInfo $file, bool $allow_links = false) : string
	{
		$real_path = ($allow_links === false) ? $file->getRealPath() : $file->getPathname();
		if (str::strpos($real_path, $request->getContentRoot()) === 0)
			return str::substr($real_path, str::strlen($request->getContentRoot()));

		if (str::strpos($real_path, DCBASE_ROOT_PATH) === 0)
			return '@dcbase_root/' . str::substr($real_path, str::strlen(DCBASE_ROOT_PATH));

		throw new Exception('Unable to resolve relative template path for file.');
	}

	public static function serveTwigFile(WebRequest $request, SplFileInfo $file) : ?WebResponse
	{
		$template_name = static::getTemplateName($request, $file);
		if ($request->variable('source', '') === 'view')
			return WebResponse::source($request, $template_name, 'twig');

		return WebResponse::page($request, $template_name);
	}

	public static function serveMarkdownFile(WebRequest $request, SplFileInfo $file) : ?WebResponse
	{
		if ($request->variable('source', '') === 'raw')
			return WebResponse::text($request, $file, 'text/markdown');

		$template_name = static::getTemplateName($request, $file);
		if ($request->variable('source', '') === 'view')
			return WebResponse::source($request, $template_name, 'markdown');

		return WebResponse::markdown($request, $template_name);
	}

	public static function serveStaticFile(WebRequest $request, SplFileInfo $file) : ?WebResponse
	{
		if ($file->isDir() || !$file->isFile() || empty($file->getExtension()))
			return null;

		try
		{
			$file_ext = str::strtolower($file->getExtension());
			$file_download = $request->variable('download', false);

			if ($file_ext === 'css')
			{
				if ($request->variable('source', '') === 'view')
					return WebResponse::source($request, str::substr($file->getRealPath(), str::strlen($request->getContentRoot())), 'css');

				return WebResponse::css($request, $file);
			}

			if ($file_ext === 'js')
			{
				if ($request->variable('source', '') === 'view')
					return WebResponse::source($request, str::substr($file->getRealPath(), str::strlen($request->getContentRoot())), 'javascript');

				return WebResponse::javascript($request, $file);
			}

			if ($file_ext === 'json')
			{
				if ($request->variable('source', '') === 'view')
					return WebResponse::source($request, str::substr($file->getRealPath(), str::strlen($request->getContentRoot())), 'json');

				return WebResponse::json($request, $file);
			}

			// provide an embedded PDF view (enabled with js for clients that support it)
			if ($file_ext === 'pdf' && $request->variable('source', '') === 'embed')
				return WebResponse::pdf($request, str::substr($file->getRealPath(), str::strlen($request->getContentRoot())));

			if ($file_ext !== 'twig' && $file_ext !== 'md' && $file_ext !== 'markdown')
				return WebResponse::file($request, $file, $file_download);

			return null;
		}
		catch (Exception $e)
		{
			return WebResponse::error($request, $file->isFile() ? 403 : 404);
		}
	}
}
