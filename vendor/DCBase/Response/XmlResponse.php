<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Response;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use DOMDocument;
use SimpleXmlElement;
use SplFileInfo;

use DCBase\WebRequest;
use DCBase\WebResponse;

use DCBase\Common\StrUtil as str;

/**
 * Web response implementation for XML
 */
class XmlResponse extends WebResponse
{
	// The DOM document, if any
	protected ?DOMDocument $xml = null;
	protected string $mime_type = 'application/xml';

	public function body($xml_data, ?string $mime_type = null, int $options = 0) : WebResponse
	{
		if ($xml_data instanceof DOMDocument)
		{
			// the trivial case
			$this->xml = $xml_data;
		}
		else
		{
			$is_loaded = false;

			$this->xml = new DOMDocument();
			$this->xml->preserveWhiteSpace = false;
			$this->xml->formatOutput = true;

			// try to get a string representation and load it
			if ($xml_data instanceof SimpleXmlElement)
			{
				$xml_data = $xml_data->asXML();

				$this->xml->loadXML($xml_data, $options);
				$is_loaded = true;
			}
			else if ($xml_data instanceof SplFileInfo || (is_string($xml_data) && str::strpos($xml_data, '<') === false))
			{
				if (!($xml_data instanceof SplFileInfo))
					$xml_data = new SplFileInfo($xml_data);

				if (!$xml_data->isReadable())
					throw new Exception('Path: "' . $xml_data->getPathname() . '" not readable.');

				if ($xml_data->isDir() || !$xml_data->isFile())
					throw new Exception('Path: "' . $xml_data->getPathname() . '" is not a file.');

				$this->xml->load($xml_data->getRealPath(), $options);
				$is_loaded = true;
			}

			if (!$is_loaded && (!is_string($xml_data) || empty($xml_data)))
				throw new Exception('Trying to generate an invalid or empty XML response.');

			if (!$is_loaded)
			{
				if (str::strpos($xml_data, '<') !== false)
					$this->xml->loadXML($xml_data, $options);
			}
		}

		// use the determined charset, so we can blame the user if it goes wrong, and provided mime type
		$this->charset = str::strtolower($this->xml->encoding);
		if ($mime_type !== null)
			$this->mime_type = $mime_type;

		return $this;
	}

	public function output() : ?string
	{
		return $this->xml ? $this->xml->saveXML() : null;
	}

	public function send(bool $exit = false) : void
	{
		// get the output here so we can enforce Content-length from server
		$output = $this->output();

		// set content type and length (bytes)
		$this->header('Content-Type', "{$this->mime_type}; charset={$this->charset}");
		$this->header('Content-Length', str::bstrlen($output));

		// lazily remove charset from all non text mime types
		if (str::substr($this->mime_type, 0, 5) !== 'text/')
			$this->header('Content-Type', "{$this->mime_type}", true);

		$this->sendHeaders();
		echo $output;

		if ($exit) exit(0);
	}
}
