<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Response;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use SplFileInfo;

use DCBase\ConfigLoader;
use DCBase\WebRequest;
use DCBase\WebResponse;

use DCBase\Common\StrUtil as str;

/**
 * Web response implementation for simple binary files
 */
class FileResponse extends WebResponse
{
	// SplFileInfo instance for the file
	protected $file;

	// These will hold settings for this file
	protected $mime_type = 'application/octet-stream';
	protected $download = true;

	// Path or a fragment that the server will use to send the file (if supported, we don't actually send content)
	protected $serve_from;

	public function body($file, bool $force_download = false, ?string $serve_from = null) : WebResponse
	{
		$this->serve_from = $serve_from;
		if (!($file instanceof SplFileInfo))
			$file = new SplFileInfo($file);

		$this->file = $file;
		if (!$this->file->isReadable() || $this->file->isDir())
			throw new Exception('File: "' . $this->file->getPathname() . '" not readable.');

		// load config
		$settings = ConfigLoader::load('MimeTypes');
		$file_ext = str::strtolower($this->file->getExtension());

		if (isset($settings[$file_ext]))
		{
			$settings = $settings[$file_ext];
			$this->mime_type = $settings['content_type'];
			$this->download = ($force_download || $settings['download']);
		}

		// remove any presets to avoid rogue headers
		$this->clearHeaders();
		return $this;
	}

	public function output() : ?string
	{
		return null;
	}

	public function send(bool $exit = false) : void
	{
		$file_size = $this->file->getSize();
		$file_mtime = $this->file->getMTime();
		$file_etag = sprintf('"%xT-%xO"', $file_mtime, $file_size);

		$offset = 0;
		$length = -1;

		$this->header('Accept-Ranges', 'bytes');
		$this->header('Cache-Control', 'public');
		$this->header('Last-Modified', gmdate('D, d M Y H:i:s', $file_mtime) .' GMT');
		$this->header('Etag', $file_etag);
		$this->header('Content-Type', $this->mime_type);
		$this->header('Content-Transfer-Encoding', 'binary');
		$this->header('Content-Length', $file_size);
		if ($this->download)
			$this->header('Content-Disposition', 'attachment; filename="'. $this->file->getBasename() .'"');

		if (defined('DCBASE_EXPOSE') && DCBASE_EXPOSE)
			$this->header('X-Powered-By', 'DCBase');

		if (!$this->download && ($this->request->hasHeader('If-Modified-Since') || $this->request->hasHeader('If-None-Match')))
		{
			if (strtotime($this->request->header('If-Modified-Since')) >= $file_mtime ||
				str::strpos(trim($this->request->header('If-None-Match')), $file_etag) !== false)
			{
				// We MUST NOT send other entity headers besides the ones that deal with caching
				$this->removeHeaders(array('Content-Type', 'Content-Transfer-Encoding', 'Content-Length'));

				$this->status_code = 304;
				$this->sendHeaders();

				if ($exit) exit(0); else return;
			}
		}

		if (!empty($this->serve_from))
		{
			if (defined('NGINX_USE_X_ACCEL_REDIRECT') && NGINX_USE_X_ACCEL_REDIRECT)
			{
				$this->header('X-Accel-Redirect', $this->serve_from);
				$this->sendHeaders();

				if ($exit) exit(0); else return;
			}
		}

		if ($this->request->hasHeader('Range') && !$this->request->hasHeader('If-Range'))
		{
			list($start, $end) = explode('-', str::substr($this->request->header('Range'), 6), 2) + array(0);
			$end = ('' === $end) ? $file_size - 1 : (int) $end;

			if ('' === $start)
			{
				$start = $file_size - $end;
				$end = $file_size - 1;
			}
			else
			{
				$start = (int) $start;
			}

			if ($start <= $end)
			{
				if ($start < 0 || $end > $file_size - 1)
				{
					$this->status_code = 416;
				}
				else if ($start !== 0 || $end !== $file_size - 1)
				{
					$length = $end < $file_size ? $end - $start + 1 : -1;
					$offset = $start;

					$this->status_code = 206;
					$this->header('Content-Range', sprintf('bytes %s-%s/%s', $start, $end, $file_size));
					$this->header('Content-Length', $end - $start + 1, true);
				}
			}
		}

		$this->sendHeaders();

		$out = fopen('php://output', 'wb');
		$file = fopen($this->file->getPathname(), 'rb');

		stream_copy_to_stream($file, $out, $length, $offset);

		fclose($out);
		fclose($file);

		if ($exit) exit(0);
	}
}
