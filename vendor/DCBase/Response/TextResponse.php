<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Response;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use SplFileInfo;

use DCBase\ConfigLoader;
use DCBase\WebRequest;
use DCBase\WebResponse;

use DCBase\Common\StrUtil as str;

/**
 * Web response implementation for text/plain
 */
class TextResponse extends WebResponse
{
	// The text response
	protected $text = null;
	protected $mime_type = 'text/plain';

	public function body($text_data, ?string $mime_type = null) : WebResponse
	{
		$this->text = $text_data;
		if (is_string($mime_type))
			$this->mime_type = $mime_type;

		if (($this->text instanceof SplFileInfo) || is_file(strval(str_replace("\0", "", $this->text))))
		{
			$file = $this->text;
			if (!($file instanceof SplFileInfo))
				$file = new SplFileInfo(strval(str_replace("\0", "", $file)));
			
			if (!$file->isReadable() || $file->isDir())
				throw new Exception('File: "' . $file->getPathname() . '" not readable.');

			// load config for lazy mime type check
			$file_ext = str::strtolower($file->getExtension());
			$settings = array();

			if (!empty($file_ext))
			{
				$settings = ConfigLoader::load('MimeTypes');

				if (!isset($settings[$file_ext]))
					throw new Exception('File: "' . $file->getPathname() . '" is not recognized type.');

				// Twig files are allowed a type mismatch from original config
				if ($file_ext !== 'twig' && $this->mime_type !== $settings[$file_ext]['content_type'])
					throw new Exception('File: "' . $file->getPathname() . '" does not have the expected file extension.');
			}

			$file_mtime = $file->getMTime();
			$file_etag = sprintf('"%xT-%xO"', $file_mtime, $file->getSize());

			$this->header('Cache-Control', 'public', true);
			$this->header('Last-Modified', gmdate('D, d M Y H:i:s', $file_mtime) .' GMT');
			$this->header('Etag', $file_etag);

			$this->removeHeaders(array('Pragma', 'Expires'));

			if (($this->request->hasHeader('If-Modified-Since') || $this->request->hasHeader('If-None-Match')))
			{
				if (strtotime($this->request->header('If-Modified-Since')) >= $file_mtime ||
					str::strpos(trim($this->request->header('If-None-Match')), $file_etag) !== false)
				{
					$this->status_code = 304;
				}
			}

			// @todo convert to FileResponse for large text files (needs some header adjustments in FileResponse)
			$this->text = ($this->status_code != 304) ? file_get_contents($file->getPathname()) : null;

			// Twig files are served with the requested mime type regardless of anything else
			if (isset($settings[$file_ext]) && $file_ext !== 'twig')
				$this->mime_type = $settings[$file_ext]['content_type'];
		}

		return $this;
	}

	public function output() : ?string
	{
		return $this->text ? $this->text : null;
	}

	public function send(bool $exit = false) : void
	{
		if ($this->status_code == 304)
		{
			// We MUST NOT send other entity headers besides the ones that deal with caching
			$this->removeHeader('Content-Type');

			$this->sendHeaders();

			if ($exit) exit(0); else return;
		}

		// get the output here so we can enforce Content-length from server
		$output = $this->output();

		// set content type and length (bytes)
		$this->header('Content-Length', str::bstrlen($output));
		$this->header('Content-Type', "{$this->mime_type}; charset={$this->charset}");

		// lazily remove charset from all non text mime types
		if (str::substr($this->mime_type, 0, 5) !== 'text/')
			$this->header('Content-Type', "{$this->mime_type}", true);

		$this->sendHeaders();
		echo $output;

		if ($exit) exit(0);
	}
}
