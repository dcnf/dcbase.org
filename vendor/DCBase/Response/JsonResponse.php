<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Response;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;
use SplFileInfo;

use DCBase\WebRequest;
use DCBase\WebResponse;

use DCBase\Common\Util;

/**
 * Web response implementation for JSON(P) requests
 */
class JsonResponse extends WebResponse
{
	// The JSON data to be encoded and sent
	protected $json = array();
	protected $mime_type = 'application/json';

	public function body($json_data) : WebResponse
	{
		if ($json_data instanceof SplFileInfo)
		{
			if (!$json_data->isReadable() || $json_data->isDir())
				throw new Exception('File: "' . $json_data->getPathname() . '" not readable.');

			$json_data = file_get_contents($json_data->getRealPath());
		}

		$this->json = $json_data;
		return $this;
	}

	public function output() : ?string
	{
		// encode json and wrap in a callback if requested
		$body = $this->json;
		if (!is_string($body))
		{
			$options = 0;
			if (Util::isDebug())
				$options |= JSON_PRETTY_PRINT;

			$body = json_encode($body, $options);
		}

		if (isset($this->request['callback']))
		{
			$callback = preg_replace('#[^a-zA-Z0-9_\\.]++#u', '', $this->request['callback']);
			if (!empty($callback))
			{
				$this->mime_type = 'application/javascript';
				$body = "// JSONP Response \n $callback($body);";
			}
		}

		return $body;
	}

	public function send(bool $exit = false) : void
	{
		// set content type to json, application/* mime types should have their own encoding specifications
		$this->header('Content-Type', "{$this->mime_type}");

		parent::send($exit);
	}
}
