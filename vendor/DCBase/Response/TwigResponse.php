<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Response;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Exception;

use Twig\Environment as TwigEnvironment;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Twig\Error\LoaderError as TwigLoaderError;

use DCBase\Twig\TextFormat\TextFormatExtension;
use DCBase\Twig\TextFormat\Parser\MarkdownParser;
use DCBase\Twig\TextFormat\Parser\MarkdownExtraParser;

use DCBase\WebRequest;
use DCBase\WebResponse;

use DCBase\Common\GitUtil;
use DCBase\Common\Util;
use DCBase\Common\StrUtil as str;

/**
 * Web response implementation for Twig
 */
class TwigResponse extends WebResponse
{
	// Twig environment
	protected $twig;

	protected $tpl_vars = array();
	protected $tpl_file = null;

	protected function __construct(WebRequest $request, int $status_code, string $charset, TwigEnvironment $env = null)
	{
		parent::__construct($request, $status_code, $charset);

		$this->tpl_vars = array(
			'content_encoding'	=> $this->charset,
			'base_url'			=> $request->getBaseURL(),
			'page_url'			=> $request->getRequestURL(),
			'path_info'			=> '/' . trim($request->getPathInfo(), '/')
		);

		if ($env !== null)
		{
			// dependency injection, environment could be created from scratch or with TwigResponse::getTwig(...), the latter of which is slightly suboptimal (duplicate WebResponse constructor)
			$this->twig = $env;
			return;
		}

		// still here, set up a fresh Twig environment
		$this->twig = new TwigEnvironment(new TwigFilesystemLoader(array($request->getContentRoot())), array(
			'charset'		=> $this->charset,
			'debug'			=> Util::isDebug(),
			'auto_reload'	=> true,
			'cache'			=> DCBASE_CACHE_PATH . 'templates/',
			'autoescape'	=> 'html'
		));

		// add namespaces, dcbase_root is potentially dangerous
		$this->twig->getLoader()->addPath(DCBASE_ROOT_PATH, 'dcbase_root');
		$this->twig->getLoader()->addPath(DCBASE_TEMPLATE_PATH, 'template');

		// Add debug functions
		$this->twig->addFunction(new TwigFunction('page_time',  function($format = true) { return Util::currentScriptTime(DCBASE_TIME_START, $format); }));
		$this->twig->addFunction(new TwigFunction('page_memory', function($format = true) { return Util::currentScriptMemory(DCBASE_MEMORY_START, $format); }));
		$this->twig->addFunction(new TwigFunction('is_debug', '\DCBase\Common\Util::isDebug'));

		// Add custom functions and filters
		$this->twig->addFilter(new TwigFilter('date_relative', '\DCBase\Common\Util::getRelativeDate'));
		$this->twig->addFilter(new TwigFilter('pathinfo', array($this, 'twigPathInfo')));
		$this->twig->addFunction(new TwigFunction('load_json', array($this, 'twigLoadJson')));

		// Git file info exposed
		$this->twig->addFunction(new TwigFunction('git_info', array($this, 'twigGitInfo')));

		// URL preprocessing function
		$this->twig->addFunction(new TwigFunction('make_url', array($this->request, 'makeURL')));

		// Rich text formatting for Twig
		$textformat = new TextFormatExtension($this->twig, array(
			// prevent cache conflicts for tags across subdomains
			'cache_salt'	=> $request->host(),
			'cache'			=> DCBASE_CACHE_PATH . '/textformat/'
		));

		$textformat->addParser(new MarkdownParser());
		$textformat->addParser(new MarkdownExtraParser());

		$this->twig->addExtension($textformat);

		// set the timezone explicitly, just in case
		$this->twig->getExtension(\Twig\Extension\CoreExtension::class)->setTimeZone(date_default_timezone_get());
	}

	public function setVariable(string $name, $value) : WebResponse
	{
		$this->tpl_vars[$name] = $value;
		return $this;
	}

	public function addRow(string $name, array $vars) : WebResponse
	{
		$this->tpl_vars[$name][] = $vars;
		return $this;
	}

	public function body($template, array $page_vars = array()) : WebResponse
	{
		$this->tpl_file = $template;
		if (!empty($page_vars))
			$this->tpl_vars = array_merge($this->tpl_vars, $page_vars);

		$this->tpl_vars['template_file'] = $this->tpl_file;
		return $this;
	}

	public function output() : ?string
	{
		return $this->twig->render($this->tpl_file, $this->tpl_vars);
	}

	public function send(bool $exit = false) : void
	{
		$this->sendHeaders();

		// if invoked from error handler, disable cache
		if (defined('DCBASE_FATAL_ERROR'))
			$this->twig->setCache(false);

		$this->twig->display($this->tpl_file, $this->tpl_vars);

		if ($exit) exit(0);
	}

	/**
	 * Allow other classes to modify the Twig environment in-place through a simple callable
	 *
	 * @return  The result of the provided $ext_fn function
	 * @throws  Exception in the event that the callable attempts to directly return the interal enviroment passed to it
	 */
	public function extend(callable $ext_fn)
	{
		$result = call_user_func_array($ext_fn, array($this->request, $this->twig));
		if ($result instanceof TwigEnvironment && $result === $this->twig)
			throw new Exception("TwigResponse::extend(...) may not be used to replace or expose the internal Twig environment outside the callable scope, use the constructor argument instead");

		return $result;
	}

	/**
	 * Twig exposed functions and filters
	 */
	public function twigPathInfo(string $path, $option)
	{
		if (is_string($option))
			$option = constant($option);

		return pathinfo($path, $option);
	}

	public function twigGitInfo($files, bool $author_info = true) : ?array
	{
		if (!GitUtil::isGit(DCBASE_ROOT_PATH))
			return null;

		if (!is_array($files))
			$files = array($files);

		$result = null;
		foreach ($files as $file)
		{
			$paths = $this->twig->getLoader()->getPaths();
			if ($file[0] === '@')
			{
				$namespace = str::substr($file, 1, str::strpos($file, '/') - 1);
				if (!empty($namespace))
				{
					$paths = $this->twig->getLoader()->getPaths($namespace);
					$file = str_replace("@$namespace/", '', $file);
				}
			}

			foreach ($paths as $path)
			{
				if (is_file($path . '/' . $file))
				{
					$info = GitUtil::getLocalFileInfo($path . '/' . $file, DCBASE_ROOT_PATH);
					if ($info === null)
						continue;

					if ($result === null || ($result['timestamp'] < $info['timestamp']))
					{
						$result = $info;
						continue;
					}
				}
			}
		}

		if (!empty($result) && !$author_info)
			unset($result['author_name'], $result['author_email']);

		return $result;
	}

	public function twigLoadJson(string $file)
	{
		$file_ext = pathinfo($file, PATHINFO_EXTENSION);
		if (empty($file_ext) || str::strtolower($file_ext) !== 'json')
			return null;

		$json_data = null;
		try
		{
			$json_data = $this->twig->getLoader()->getSourceContext($file)->getCode();
		}
		catch (TwigLoaderError $e)
		{
			return null;
		 }

		return json_decode($json_data, true);
	}

	/**
	 * Return a copy of Twig environment matching this request
	 */
	public static function getTwig(WebRequest $request, int $status_code = 200, ?string $charset = null) : TwigEnvironment
	{
		$tmp = WebResponse::create($request, WebResponse::TWIG_RESPONSE, $status_code, $charset);
		return $tmp->twig;
	}
}
