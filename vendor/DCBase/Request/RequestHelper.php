<?php
/**
*
* @package dcbase
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

declare(strict_types = 1);

namespace DCBase\Request;

/**
* @ignore
*/
if (!defined('IN_DCBASE')) exit;

use Normalizer;

use DCBase\Common\StrUtil as str;

/**
 * Static class for helper functions for request handling.
 *
 * The methods in this class are stateless utility method related to incoming
 * request processing.
 */
class RequestHelper
{
	/// private final constructor so no instances can be created
	final private function __construct() { }

	/**
	 * Typecast helper, makes sure user input is of correct type and (possibly) encoding 
	 */
	public static function typecast(&$value, string $type, bool $multibyte = false) : void
	{
		settype($value, $type);

		if ($type === 'array')
		{
			// I don't believe using PHP's built in support for arrays on request data is generally a good idea, 
			// leaking implementation details and all that, so cutting corners here is fine.
			foreach ($value as $arr_key => $arr_value)
			{
				static::typecast($arr_value, 'string', $multibyte);
				static::typecast($arr_key, (is_int($arr_key) || ctype_digit($arr_key)) ? 'int' : 'string', $multibyte);

				$value[$arr_key] = $arr_value;
			}
		}
		else if ($type === 'string')
		{
			$value = trim(str_replace(array("\r\n", "\r", "\0"), array("\n", "\n", ''), $value));
			if ($multibyte)
			{
				if (!Normalizer::isNormalized($value))
					$value = (string) Normalizer::normalize($value);
			}

			if (!empty($value))
			{
				if (!$multibyte)
				{
					$value = preg_replace('/[\x80-\xFF]/', '?', $value);
				}
				else if ($multibyte && !preg_match('/^./u', $value))
				{
					$value = '';
				}
			}
		}
	}

	/**
	 * Regex helper, builds actual regex from our dynamic lookup patterns
	 *
	 * Pattern format inspired by Laravel routing
	 */
	public static function buildPathExpression(&$pattern, ?array $regex = null) : bool
	{
		$pattern = rtrim($pattern, '/');
		if (empty($pattern))
			return false;

		$pattern = preg_replace('#[.\\+*?[^\\]${}=!|]#', '\\\\$0', $pattern);

		if (str::strpos($pattern, '(') !== false)
		{
			// optional parts
			$pattern = str_replace(array('(', ')'), array('(?:', ')?'), $pattern);
		}

		$pattern = str_replace(array('<', '>'), array('(?P<', '>[^/.,;?\n]++)'), $pattern);

		if ($regex !== null)
		{
			$search = $replace = array();
			foreach ($regex as $key => $value)
			{
				$search[] = "<$key>[^/.,;?\\n]++";
				$replace[] = "<$key>$value";
			}

			// replace with the user-specified regex
			$pattern = str_replace($search, $replace, $pattern);
		}

		$pattern = '#^'. $pattern .'$#uD';
		return true;
	}
}
