<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat;

use Twig\Compiler as TwigCompiler;
use Twig\Node\Node as TwigNode;
use Twig\Node\Expression\AbstractExpression as TwigAbstractExpression;

use DCBase\Twig\TextFormat\TextFormatExtension;

/**
 * Implements node for TextFormat extension that loads metadata from a file into the current context
 */
class MetaFileNode extends TwigNode
{
	public function __construct(TwigAbstractExpression $expr, int $lineno, ?string $tag = null)
	{
		parent::__construct(array('expr' => $expr), array(), $lineno, $tag);
	}

	/**
	 * Compiles to PHP
	 */
	public function compile(TwigCompiler $compiler) : void
	{
		$compiler->addDebugInfo($this);

		$compiler
		  ->write('{' . PHP_EOL)
		  ->indent();

		$compiler
		  ->write('$parser = $this->env->getExtension(')
		  ->repr(TextFormatExtension::class)
		  ->raw(');' . PHP_EOL);

		$compiler
	      ->write('$source_file = strval(')
	      ->subcompile($this->getNode('expr'))
	      ->raw(');' . PHP_EOL);

		$this->addLoadMetadata($compiler);

		$compiler
		  ->outdent()
		  ->write('}'. PHP_EOL);
	}

	protected function addLoadMetadata(TwigCompiler $compiler) : void
	{
		$compiler
		  ->write('$metadata = $parser->loadMetadataFromFile($source_file);'. PHP_EOL)
		  ->write('foreach ($metadata as $var_name => $var_value) {' . PHP_EOL)
		  ->indent()
	      ->write('if (!isset($context[$var_name]) || $context[$var_name] === null || (gettype($context[$var_name]) === gettype($var_value))) { $context[$var_name] = $var_value; }' . PHP_EOL)
		  ->outdent()
		  ->write('}'. PHP_EOL);
	}
}
