<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat;

use Twig\TokenParser\AbstractTokenParser as TwigAbstractTokenParser;
use Twig\Token as TwigToken;
use Twig\Node\Node as TwigNode;

use DCBase\Twig\TextFormat\MetaFileNode;

/**
 * TokenParser implementation for metadata tags
 */
class MetaFileTokenParser extends TwigAbstractTokenParser
{
	protected $tag_name;

	public function __construct($tag_name = 'metafile')
	{
		$this->tag_name = $tag_name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function parse(TwigToken $token) : TwigNode
	{
		$no_cache = false;
		$stream = $this->parser->getStream();

		$expr = $this->parser->getExpressionParser()->parseExpression();

		$stream->expect(TwigToken::BLOCK_END_TYPE);

		return new MetaFileNode($expr, $token->getLine(), $this->tag_name);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTag() : string
	{
		return $this->tag_name;
	}
}
