<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat;

use Twig\Compiler as TwigCompiler;
use Twig\Node\IncludeNode as TwigIncludeNode;
use Twig\Node\Expression\AbstractExpression as TwigAbstractExpression;

use DCBase\Twig\TextFormat\TextFormatExtension;

use DCBase\Common\StrUtil as str;

/**
 * Include node implementation for TextFormatExtension
 */
class IncludeNode extends TwigIncludeNode
{
	public function __construct(TwigAbstractExpression $expr, ?TwigAbstractExpression $variables = null, ?TwigAbstractExpression $depends = null,
		bool $no_cache = false, bool $only = false, bool $ignore_missing = false, int $lineno, ?string $tag = null)
	{
		parent::__construct($expr, $variables, $only, $ignore_missing, $lineno, $tag);

		if ($depends !== null)
			$this->setNode('depends_on', $depends);

		$this->setAttribute('nocache', (bool) $no_cache);
	}

	/**
	 * Compiles to PHP
	 */
	public function compile(TwigCompiler $compiler) : void
	{
		$compiler->addDebugInfo($this);

		$compiler
		  ->write(($this->getAttribute('ignore_missing') ? 'try' . PHP_EOL  : '') . '{' . PHP_EOL)
		  ->indent();

		$compiler
		  ->write('$parser = $this->env->getExtension(')
		  ->repr(TextFormatExtension::class)
		  ->raw(');' . PHP_EOL);

		$compiler
		  ->write('$source_file = strval(')
		  ->subcompile($this->getNode('expr'))->raw(');' . PHP_EOL);

		$compiler
		  ->write('$cache_info = ')
		  ->repr(TextFormatExtension::getContentCacheInfo($this, true))
		  ->raw(';' . PHP_EOL);

		$this->addCacheInfoUpdates($compiler);

		$this->addLoadContent($compiler);

		$compiler
		  ->outdent()
		  ->write('}'. PHP_EOL);

		 if ($this->getAttribute('ignore_missing'))
		 {
			$compiler
			  ->write('catch (LoaderError $e)' . PHP_EOL)
			  ->write('{'. PHP_EOL)
			  ->indent()
			  ->write('// ignore missing file' . PHP_EOL)
			  ->outdent()
			  ->write('}' . PHP_EOL);
		 }
	}

	protected function addCacheInfoUpdates(TwigCompiler $compiler) : void
	{
		$compiler->write('if (!empty($cache_info)) {' . PHP_EOL)
		  ->indent()
		  ->write('$cache_info["source_file"] = $source_file;' . PHP_EOL);

		if ($this->hasNode('depends_on'))
		{
			$compiler
			  ->write('$cache_info["dependencies"] = ')
			  ->subcompile($this->getNode('depends_on'))
			  ->raw(';' . PHP_EOL);
		}

		$compiler
		  ->outdent()
		  ->write('}' . PHP_EOL);
	}

	protected function addLoadContent(TwigCompiler $compiler) : void
	{
		$compiler
		  ->write('$content = $parser->loadContentFromFile($source_file, ')
		  ->repr($this->getTemplateName())
		  ->raw(', ')
		  ->repr($this->getTemplateLine())
		  ->raw(', ');

		// note: this works as long as Twig's display(...) and render(...) method signatures do not change
		$this->addTemplateArguments($compiler);

		$compiler
		  ->raw(');'. PHP_EOL)
		  ->write('echo $parser->parseContent(')
		  ->repr($this->getNodeTag())
		  ->raw(', $content, true, $cache_info);' . PHP_EOL);
	}
}
