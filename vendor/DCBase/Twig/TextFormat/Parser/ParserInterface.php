<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat\Parser;

/**
 * Trivial parser interface for TextFormatExtension::parseContent()
 */
interface ParserInterface
{
	/**
	 * Return an array of one or more tag names for this parser (converted to lowercase)
	 */
	public function getNames() : array;

	/**
	 * Returns the parsed content string as HTML
	 */
	public function parseContent($content) : string;

	/**
	 * Return the internal parser instance
	 */
	public function getImplementation();
}
