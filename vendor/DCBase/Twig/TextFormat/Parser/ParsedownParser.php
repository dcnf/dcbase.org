<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat\Parser;

use DCBase\Twig\TextFormat\Parser\ParserInterface;

/**
 * Trivial parser implementation for Parsedown
 */
abstract class ParsedownParser implements ParserInterface
{
	// Holds the internal parser instance
	protected $parser = null;

	protected function __construct($parsedown_class)
	{
		$this->parser = new $parsedown_class();
		$this->parser->setMarkupEscaped(false);
		$this->parser->setBreaksEnabled(false);

		// @see Parsedown #358 (https://github.com/erusev/parsedown/issues/358) and ParsedowExtra #77 (https://github.com/erusev/parsedown-extra/issues/77)
		$this->parser->setUrlsLinked(false);

		// non-standard method implemented by DCBase\Parsedown\ParsedownExtensionTrait
		$this->parser->setDefaultTableStyle('.table');
		$this->parser->setDefaultBlockquoteStyle('.blockquote');
	}

	/**
	 * {@inheritdoc}
	 */
	public abstract function getNames() : array;

	/**
	 * {@inheritdoc}
	 */
	public function parseContent($content) : string
	{
		return $this->parser->text($content);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getImplementation()
	{
		return $this->parser;
	}
}
