<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat\Parser;

use DCBase\Twig\TextFormat\Parser\ParsedownParser;
use DCBase\Parsedown\ParsedownExtra;

/**
 * Trivial parser implementation for Markdown Extra using Parsedown;
 */
class MarkdownExtraParser extends ParsedownParser
{
	public function __construct()
	{
		parent::__construct(ParsedownExtra::class);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNames() : array
	{
		return array('MarkdownExtra');
	}
}
