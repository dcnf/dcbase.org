<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat;

use Twig\Compiler as TwigCompiler;
use Twig\Node\Node as TwigNode;
use Twig\Node\NodeOutputInterface as TwigNodeOutputInterface;
use Twig\Node\Expression\AbstractExpression as TwigAbstractExpression;

use DCBase\Twig\TextFormat\TextFormatExtension;

/**
 * Inline node (ie. block with content) implementation for TextFormatExtension
 */
class InlineNode extends TwigNode implements TwigNodeOutputInterface
{
	public function __construct(TwigNode $body, ?TwigAbstractExpression $depends = null, bool $no_cache = false, int $lineno, ?string $tag = null)
	{
		$nodes = array('body' => $body);
		if ($depends !== null)
			$nodes['depends_on'] = $depends;

		parent::__construct($nodes, array('nocache' => $no_cache), $lineno, $tag);
	}

	/**
	 * Compiles to PHP
	 */
	public function compile(TwigCompiler $compiler) : void
	{
		$compiler->addDebugInfo($this);

		$compiler
		  ->write('{' . PHP_EOL)
		  ->indent();

		$compiler
		  ->write('ob_start();' . PHP_EOL)
		  ->subcompile($this->getNode('body'))
		  ->write('$content = ob_get_clean();' . PHP_EOL);

		$compiler
		  ->write('$cache_info = ')
		  ->repr(TextFormatExtension::getContentCacheInfo($this, true))
		  ->raw(';' . PHP_EOL);

		$this->addCacheInfoUpdates($compiler);

		$this->addParseContent($compiler);

		$compiler
		  ->outdent()
		  ->write('}'. PHP_EOL);
	}

	protected function addCacheInfoUpdates(TwigCompiler $compiler) : void
	{
		if (!$this->hasNode('depends_on'))
			return;

		$compiler
		  ->write('if (!empty($cache_info)) {' . PHP_EOL)
		  ->indent()
	      ->write('$cache_info["dependencies"] = ')->subcompile($this->getNode('depends_on'))->raw(';' . PHP_EOL);

		$compiler
		  ->outdent()
		  ->write('}' . PHP_EOL);
	}

	protected function addParseContent(TwigCompiler $compiler) : void
	{
		$compiler
		  ->write('echo $this->env->getExtension(')->repr(TextFormatExtension::class)
		  ->raw(')->parseContent(')->repr($this->getNodeTag())
		  ->raw(', $content, false, $cache_info);' . PHP_EOL);
	}
}
