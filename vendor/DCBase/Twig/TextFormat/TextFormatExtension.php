<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat;

use Exception;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use ReflectionObject;
use Traversable;

use Twig\TwigFilter;
use Twig\Source as TwigSource;
use Twig\Environment as TwigEnvironment;
use Twig\Error\Error as TwigError;
use Twig\Extension\AbstractExtension as TwigAbstractExtension;
use Twig\Node\Node as TwigNode;

use DCBase\Twig\TextFormat\TextFormatTokenParser;
use DCBase\Twig\TextFormat\MetaFileTokenParser;
use DCBase\Twig\TextFormat\Parser\ParserInterface;

use DCBase\Common\Util;
use DCBase\Common\StrUtil as str;

/**
 * TextFormatExtension provides support for formatting rich text through 3rd party parsers.
 */
class TextFormatExtension extends TwigAbstractExtension
{
	// block counter for inline cached blocks
	private static $content_block_id = 0;

	private $env;
	private $options = array();
	private $parsers = array();
	private $parsers_updated = 0;

	/**
	 * Constructor
	 */
	public function __construct(TwigEnvironment $env, array $options = null)
	{
		$this->env = $env;

		if (!empty($options))
		{
			$this->options = array_replace($this->options, $options);

			if (isset($options['cache']))
			{
				$this->setCache($options['cache']);

				if (!isset($this->options['cache_salt']))
					$this->options['cache_salt'] = '_main';
			}
		}
	}

	public function setCache($directory)
	{
		if (!is_string($directory))
			throw new Exception('TextFormatExtension (Twig): cache for this extension must be configured using a directory path.');

		$directory = rtrim($directory, '/\\');
		if (!is_dir($directory))
		{
			if (mkdir($directory, 0755, true) === false && !is_dir($directory))
				throw new Exception('TextFormatExtension (Twig): could not create cache directory.');
		}
		else if (!is_writable($directory))
			throw new Exception('TextFormatExtension (Twig): cache path not writebale.');

		$this->options['cache'] = $directory;
	}

	/**
	 * Register a named tag and filter using a parser instance 
	 */
	public function addParser(ParserInterface $parser) : bool
	{
		foreach ($parser->getNames() as $name)
			$this->parsers[str::strtolower($name)] = $parser;

		return true;
	}

	/**
	 * Return parser instance based on the used tag
	 */
	protected function getParser($parser_name) : ParserInterface
	{
		if (!isset($this->parsers[$parser_name]))
			throw new Exception('TextFormatExtension (Twig): unknown parser, "'. $parser_name .'".');

		return $this->parsers[$parser_name];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFilters() : array
	{
		static $filters = array();

		if (empty($filters))
		{
			$that = $this;
			$parser_names = array_keys($this->parsers);
			foreach ($parser_names as $name)
				$filters[] =  new TwigFilter($name, function ($content) use ($that) { return $that->parseContent($name, $content, true); }, array('is_safe' => array('html')));
		}

		return $filters;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTokenParsers() : array
	{
		static $token_parsers = array();

		if (empty($token_parsers))
		{
			$token_parsers[] = new MetaFileTokenParser();

			$parser_names = array_keys($this->parsers);
			foreach ($parser_names as $name)
				$token_parsers[] = new TextFormatTokenParser($name);
		}

		return $token_parsers;
	}

	/**
	 * Loads and caches content from file (caching strategy: runtime)
	 *
	 * @param  string	$source_file	The file to load
	 * @param  string	$template_name	The name of the template this method is called from
	 * @param  int		$line			The template line that calls this method
	 * @param  array	$context		The template context, used if contents are to be rendered by Twig
	 *
	 * @return string					Content ready to be parsed
	 */
	public function loadContentFromFile($source_file, $template_name, $line = null, array $context = array()) : string
	{
		$tpl_ext = (string) pathinfo($template_name, PATHINFO_EXTENSION);
		$ext_len = 0;

		if (!empty($tpl_ext))
		{
			$tpl_ext = ".$tpl_ext";
			$ext_len = str::strlen($tpl_ext);
		}

		try
		{
			$content = null;
			if (str::substr($source_file, 0 - $ext_len, $ext_len) === $tpl_ext)
			{
				$content = $this->env->load($source_file)->render($context);
			}
			else
			{
				$content = $this->env->getLoader()->getSourceContext($source_file)->getCode();
			}
		}
		catch (TwigError $e)
		{
			// Error handling similar to Twig\Template::loadTemplate()
			if (!$e->getSourceContext())
				$e->setSourceContext(new TwigSource('', $template_name));

			if ($e->getTemplateLine() > 0)
				throw $e;

			if (!$line)
			{
				$e->guess();
			}
			else
			{
				$e->setTemplateLine($line);
			}

			throw $e;
		}

		return $content;
	}

	public function loadMetadataFromFile($source_file) : array
	{
		$content = $this->env->getLoader()->getSourceContext($source_file)->getCode();

		$metadata = array();
		if (str::strpos($content, '<!-- metadata:') === 0 && ($end = str::strpos($content, '-->', 14)) !== false)
			$metadata = json_decode(str::substr($content, 14, $end - 15), true);

		if (empty($metadata) && str::strpos($content, '{# metadata:') === 0 && ($end = str::strpos($content, '#}', 12)) !== false)
			$metadata = json_decode(str::substr($content, 12, $end - 13), true);

		return $metadata ? (array) $metadata : array();
	}

	/**
	 * Parse content to HTML
	 */
	public function parseContent($parser, $content, $file_or_filter, array $cache_info = null) : string
	{
		$cached = $this->getCachedContent($cache_info);
		if ($cached !== null)
			return $cached;

		// This is just for convenience, requires appropriate CSS style to exist
		$content = str_replace('<!-- pagebreak -->', '<div class="pagebreak"> </div>', $content);

		$parser = $this->getParser($parser);
		if ($file_or_filter)
		{
			// This is for directly included files and for filters (cache one, not the other)
			//   Note: no need to do whitespace correction
			if (empty($cache_info) || $cache_info['source_file'] === false)
				return $parser->parseContent($content);

			$parsed_content = $parser->parseContent($content);
			$this->putCachedContent($parsed_content, $cache_info);

			return $parsed_content;
		}

		$whitespace = array();
		preg_match("#^\s*#", $content, $whitespace);

		$lines = explode("\n", str_replace(array("\r\n", "\r"), "\n", $content));
		$content = preg_replace("#^{$whitespace[0]}#", '', $lines);
		$content = join("\n", $content);

		$parsed_content = $parser->parseContent($content);
		$this->putCachedContent($parsed_content, $cache_info);

		return $parsed_content;
	}

	/**
	 * Cache functions
	 */
	public static function getContentCacheInfo(TwigNode $node, $new_key = false) : ?array
	{
		if ($new_key)
			++static::$content_block_id;

		if ($node->getAttribute('nocache'))
			return null;

		return array(
			'source_file'		=> false,
			'parent_template'	=> $node->getTemplateName(),
			'dependencies'		=> false,
			'block_id'			=> static::$content_block_id,
			'parser'			=> $node->getNodeTag()
		);
	}

	protected function getCachedContent(array &$cache_info = null) : ?string
	{
		if (empty($cache_info) || !isset($this->options['cache']) || !isset($cache_info['block_id']))
			return null;

		$source_file = ($cache_info['source_file'] === false) ? $cache_info['parent_template'] : $cache_info['source_file'];
		if (!isset($cache_info['cache_key']))
			$cache_info['cache_key'] = sha1("{$source_file}@{$this->options['cache_salt']}" . (($cache_info['source_file'] === false) ? '.' . $cache_info['block_id'] : ''));

		$cache_file = $this->options['cache'] . '/' . $cache_info['cache_key'] . '.ccache';
		if (!is_file($cache_file) || !$this->isCacheFresh($cache_info, filemtime($cache_file)))
			return null;

		$cache_content = file_get_contents($cache_file);
		return ($cache_content !== false) ? $cache_content : null;
	}

	protected function isCacheFresh($cache_info, $time) : bool
	{
		if (!$this->env->isTemplateFresh($cache_info['parent_template'], $time))
			return false;

		if ($cache_info['source_file'] !== false && !$this->env->isTemplateFresh($cache_info['source_file'], $time))
			return false;

		// look through manually provided list of dependencies
		if ($cache_info['dependencies'] !== false && !empty($cache_info['dependencies']))
		{
			if (!is_array($cache_info['dependencies']) && !($cache_info['dependencies'] instanceof Traversable))
				$cache_info['dependencies'] = array((string) $cache_info['dependencies']);

			foreach ($cache_info['dependencies'] as $template_file)
			{
				if (!$this->env->isTemplateFresh($template_file, $time))
					return false;
			}
		}

		// if the parser implementations have changed try to invalidate cache
		if ($this->parsers_updated === 0)
		{
			foreach ($this->parsers as $parser)
			{
				$parser = $parser->getImplementation();
				if (!is_object($parser))
					continue;

				$ro = new ReflectionObject($parser);
				if (file_exists($ro->getFileName()) && ($update_time = filemtime($ro->getFileName())) > $this->parsers_updated)
					$this->parsers_updated = $update_time;
			}
		}

		return $this->parsers_updated <= $time;
	}

	protected function putCachedContent($parsed_content, array &$cache_info = null) : ?int
	{
		if (empty($cache_info) || !isset($this->options['cache']) || !isset($cache_info['block_id']))
			return null;

		$source_file = ($cache_info['source_file'] === false) ? $cache_info['parent_template'] : $cache_info['source_file'];
		if (!isset($cache_info['cache_key']))
			$cache_info['cache_key'] = sha1("{$source_file}@{$this->options['cache_salt']}" . (($cache_info['source_file'] === false) ? '.' . $cache_info['block_id'] : ''));

		$debug_info = '';
		$cache_file = $this->options['cache'] . '/' . $cache_info['cache_key'] . '.ccache';

		$cache_tag = '';
		if ($this->env->isDebug())
		{
			$debug_info = "($source_file, {$cache_info['block_id']})";
			$cache_tag = '<!-- Generated on  '. date('Y-m-d - g:ia') ." with parser \"{$cache_info['parser']}\" $debug_info". PHP_EOL .' -->';
		}

		return Util::stringToFile($cache_file, $cache_tag . $parsed_content);
	}

	public function pruneContentCache($stale_time = null) : bool
	{
		if (!isset($this->options['cache']))
			return false;

		if ($stale_time === null)
			$stale_time = 3600 * 96; // 96 hours, ie. 4 days

		$stale_time = time() - $stale_time;

		foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->options['cache'] . '/'), RecursiveIteratorIterator::LEAVES_ONLY) as $file)
		{
			if ($file->isFile() && $file->getATime() < $stale_time)
				@unlink($file->getPathname());
		}

		return true;
	}
}
