<?php
/**
*
* @package dcbase-twig-textformat
* @copyright (c) 2020 Direct Connect Network Foundation / www.dcbase.org
* @license https://www.dcbase.org/DCBase/LICENSE GNU General Public License v2
*
*/

namespace DCBase\Twig\TextFormat;

use Twig\TokenParser\IncludeTokenParser as TwigIncludeTokenParser;
use Twig\Token as TwigToken;
use Twig\Node\Node as TwigNode;

use DCBase\Twig\TextFormat\InlineNode;
use DCBase\Twig\TextFormat\IncludeNode;

/**
 * TokenParser implementation for TextFormatExtension
 */
class TextFormatTokenParser extends TwigIncludeTokenParser
{
	protected $tag_name;

	public function __construct($tag_name)
	{
		$this->tag_name = $tag_name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function parse(TwigToken $token) : TwigNode
	{
		$no_cache = false;
		$depends = null;
		$stream = $this->parser->getStream();

		if ($stream->nextIf(TwigToken::BLOCK_END_TYPE) || ($no_cache = (bool) $stream->nextIf(TwigToken::NAME_TYPE, 'nocache')) || ($depends = $stream->nextIf(TwigToken::NAME_TYPE, 'depends')))
		{
			if ($depends)
			{
				$stream->expect(TwigToken::NAME_TYPE, 'on');
				$depends = $this->parser->getExpressionParser()->parseExpression();
			}

			if ($no_cache || $depends)
				$stream->expect(TwigToken::BLOCK_END_TYPE);

			$body = $this->parser->subparse(array($this, 'decideBlockEnd'), true);
			$stream->expect(TwigToken::BLOCK_END_TYPE);

			return new InlineNode($body, $depends, $no_cache, $token->getLine(), $this->tag_name);
		}
		else
		{
			$expr = $this->parser->getExpressionParser()->parseExpression();

			if ($stream->nextIf(TwigToken::NAME_TYPE, 'nocache'))
				$no_cache = true;

			if (!$no_cache && $stream->nextIf(TwigToken::NAME_TYPE, 'depends'))
			{
				$stream->expect(TwigToken::NAME_TYPE, 'on');
				$depends = $this->parser->getExpressionParser()->parseExpression();
			}

			// standard Twig include tag options (note: this looks for TwigToken::BLOCK_END_TYPE for us)
			list($variables, $only, $ignore_missing) = $this->parseArguments();

			return new IncludeNode($expr, $variables, $depends, $no_cache, $only, $ignore_missing, $token->getLine(), $this->tag_name);
		}
	}

	/**
	 * Test for end of block
	 */
	public function decideBlockEnd(TwigToken $token) : bool
	{
		return $token->test("end{$this->tag_name}");
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTag() : string
	{
		return $this->tag_name;
	}
}
